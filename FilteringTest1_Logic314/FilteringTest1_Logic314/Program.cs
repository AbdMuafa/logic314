﻿using System;

namespace FilteringTest1_Logic314
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //Console.WriteLine("Hello World!");
            //soal1();
            //soal2();
            //soal3();
            //soal4()
            //soal5();
            //soal6();
            //soal7();
            //soal8();
            //soal9();
            soal10();
            Console.ReadKey();
        }

        static void soal10()
        {

            // kelipatan 3 - 1, kelipatan 4 bagi 2
            Console.Write("\tMasukkan panjang : ");
            int panjang = int.Parse(Console.ReadLine());
            int genap = 2, ganjil = 1, totalgenap = 0, totalganjil = 0;
            Console.Write("\t");

            Console.WriteLine("\n\tkelipatan 4 / 2: ");
            Console.Write("\t");
            for (int i = 0; i < panjang; i++)
            {

                    if (i % 4 == 0 || i < 10) { 
                        Console.Write((i/2) + " ");
                    }
                
            }
            //Console.WriteLine("\n\tTotal : " + totalgenap);

            Console.WriteLine("\n\tkelipatan 3 - 1: ");
            Console.Write("\t");
            for (int i = 0; i < panjang; i++)
            {
                totalganjil += ganjil - 1;
                if ( i % 3 == 0 && i != 0 && i <= panjang)
                {
                    i += 1;
                    Console.Write(i-2 + " ");
                }
            }
            //Console.WriteLine("\n\tTotal Ganjil: " + totalganjil);

        }

        static void soal9()
        {// diskon huruf vokal e 10ribu, a diskon 20ribu
            Console.WriteLine("Kata kunci nama buah beserta harga");
            Console.WriteLine("-. Semangka 30 ribu / kg");
            Console.WriteLine("-. Pir 10 ribu / kg");
            Console.Write("Masukkan nama buah : ");
            string huruf = Console.ReadLine().ToLower();
            string charVokal = "";

            foreach (char c in huruf)
            {
                if ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z'))
                {
                    if (c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u')
                    {
                        charVokal += c;
                    }

                }
            }

            Console.WriteLine($"\nHarga buah {huruf.ToUpper()}: {charVokal.Length * 10000} ");

            //int hargaPir = 10000, semangka = 30000, alpukat = 25000, mangga = 20000;
            //Console.WriteLine("Kata kunci nama buah beserta harga");
            //Console.WriteLine("-. Semangka 30 ribu / kg");
            //Console.WriteLine("-. Pir 10 ribu / kg");
            //Console.WriteLine("-. Alpukat 25 ribu / kg");
            //Console.WriteLine("-. Mangga 30 ribu / kg");

            //Console.Write("Masukkan nama buah : ");
            //string namaBuah = Console.ReadLine().ToLower();
            //if (namaBuah == "Pir")
            //{
            //    Console.WriteLine($"Harga buah {hargaPir} / kg");
            //}else if (namaBuah == "semangka")
            //{
            //    Console.WriteLine($"Harga buah {semangka} / kg");
            //}
            //else if (namaBuah == "alpukat")
            //{
            //    Console.WriteLine($"Harga buah {alpukat} / kg");
            //}
            //else if (namaBuah == "mangga")
            //{
            //    Console.WriteLine($"Harga buah {mangga} / kg");
            //}
            //else
            //{
            //    Console.WriteLine("Buah belum tersedia");
            //}
        }
        static void soal8()
        {
            Console.Write("Masukkan tanggal mulai (dd/mm/yyyy) : ");
            DateTime dateStart = DateTime.Parse(Console.ReadLine());
            Console.Write("Masukkan berapa hari lagi akan ujian : ");
            int ujian = int.Parse(Console.ReadLine());
            Console.Write("Tanggal hari libur : ");
            int[] dateLibur = Array.ConvertAll(Console.ReadLine().Split(","), int.Parse);

            DateTime dateSelesai = dateStart;
            for (int i = 0; i < ujian; i++)
            {
                if (i > 0)
                {
                    dateSelesai = dateSelesai.AddDays(1);
                }
                if (dateSelesai.DayOfWeek == DayOfWeek.Saturday)
                {
                    dateSelesai = dateSelesai.AddDays(2);
                }
                for (int j = 0; j < dateLibur.Length; j++)
                {
                    if (dateSelesai.Day == dateLibur[j])
                    {
                        dateSelesai = dateSelesai.AddDays(1);
                        if (dateSelesai.DayOfWeek == DayOfWeek.Saturday)
                        {
                            dateSelesai = dateSelesai.AddDays(2);
                        }
                    }
                }
            }
            DateTime dateUjian = dateSelesai.AddDays(1);
            if (dateUjian.DayOfWeek == DayOfWeek.Saturday)
            {
                dateUjian = dateUjian.AddDays(2);
            }
            Console.WriteLine("\tKelas akan ujian pada tanggal : " + dateUjian.ToString("dddd, dd/MMMM/yyyy"));
        }
        //static void soal8()
        //{
        //    Console.Write("hari x : ");
        //    int x = int.Parse(Console.ReadLine());
        //    Console.Write("hari y : ");
        //    int y = int.Parse(Console.ReadLine());
        //    Console.Write("Masukkan tanggal libur yang sama (dd/mm/yyyy) : ");
        //    DateTime dateLibur = DateTime.Parse(Console.ReadLine());



        //}

        static void soal7()
        {
            Console.WriteLine("\t===== Pangrams =====");
            Console.Write("\tMasukkan array angka : ");
            string angka = Console.ReadLine().ToLower();
            Console.Write("\nMasukkan kalimat : ");
            string alfabet = Console.ReadLine().ToLower();
            bool pangram = false;

            int a = 1, b = 2, c = 5, d = 4;
                if (a == 1 && b == 2 && c == 3)
                {
                    pangram = true;
                }
                else
                {
                    pangram = false;
                }
                Console.Write(pangram + " ");

            //foreach (char c in alfabet)
            //{
            //    if (!angka.Contains(c))
            //    {
            //        pangram = true;
            //    }
            //    else
            //    {
            //        pangram = false;
            //    }
        }

        static void soal6()
        {
            Console.WriteLine("Ket : \nN = Naik, \nT = Turun");
            Console.Write("Masukkan kata : ");
            string huruf = Console.ReadLine().ToLower();
            string gunung = "", lembah = "";

            int start = 0;

            foreach (char c in huruf)
            {
                if (c == 'N' || c == 'n')
                {
                    gunung += c;
                }
                else
                {
                    lembah += c;
                }
            }
            Console.WriteLine($"Gunung yang sudah dilewati {gunung.Length}");
            Console.WriteLine($"Lembah yang sudah dilewati {lembah.Length}");

        }

        static void soal5()
        {

            /* Ada berapa banyak bangun persegi dengan panjang sisi n cm yang dapat digunakan untuk membuat sebuah kubus dengan panjang salah satu rusuknya m cm ?*/
            Console.Write("Masukkan persegi panjang sisi (cm) : ");
            int n = int.Parse(Console.ReadLine());
            Console.Write("Masukkan rusuk (cm) : ");
            int m = int.Parse(Console.ReadLine());
            // 3 banding 1 kubus 9 kotak kecil dan 6 sisi
            // 25/6 

            //int persegi = n / m ;
            //int kubus = persegi / m;
            //if (persegi < kubus)
            //{
            //    Console.
            //}
            //int total = kubus + persegi;

            //Console.WriteLine( kubus + " "+ persegi);

            //double luasPersegi = Math.P(n, 2);
            //Console.WriteLine("Luas persegi= " + luasPersegi + " cm^2");
            //double luasKubus = 6 * (Math.pow(m, 2));
            //Console.WriteLine("Luas permukaan kubus= " + luasKubus + " cm^2");
            //int banyakBangun = (int)(luasKubus / luasPersegi);
            //Console.WriteLine("Banyak bangun persegi untuk membuat sebuah kubus= " + banyakBangun + " buah");

        }
        static void soal4()
        {
            //tampilkan 10 angka kelipatan 3 yang genap, contoh 6,12,18,24,
            Console.Write("masukkan jumlah angka : ");
            int angka = int.Parse(Console.ReadLine());
            for (int i = 1; i <= angka; i++)
            {

                for (int j = 1; j <= angka; j++)
                {
                    if (j % 3 == 0)
                    {
                        if (j % 2 == 0)
                        {
                            Console.WriteLine(i);
                        }
                    }
                }


            }
        }
        static void soal1()
        {
            //5 stik es loli dapat ditukar menjadi 1 buah es loli gratis
            Console.WriteLine("--soal no 1 es stik--");
            Console.Write("Masukkan uang : Rp.");
            int uang = int.Parse(Console.ReadLine());

            int esLoli = uang / 1000;
            //double gratis = Math.Round(esLoli / 5, MidpointRounding.ToPositiveInfinity); ;
            int gratis = esLoli / 5;
            //int sisaStik = gratis + esLoli;
            int hasil = esLoli + gratis;
            //9=11, 10=13, 11=14, 12=15, 13=16, 14=17

            if (esLoli % 5 == 0 || esLoli >= 5)
            {
                gratis += 1;
                Console.WriteLine($"Membawa uang {uang} mendapatkan {hasil} stik");
            }
            else
            {
                Console.WriteLine($"Membawa uang {uang} mendapatkan {hasil} stik");
            }
        }
        static void soal2()
        {
            Console.Write("Masukkan kata : ");
            string huruf = Console.ReadLine().ToLower();
            string charVokal = "", charKonsonan = "";

            foreach (char c in huruf)
            {
                char[] charvokAL = charVokal.ToCharArray();
                if ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z'))
                {
                    if (c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u')
                    {
                        charVokal += c;
                    }
                    else
                    {
                        charKonsonan += c;
                    }
                }
            }
            Console.WriteLine("\nHuruf : " + charVokal + " - " + charKonsonan);
        }
        static void soal3()
        {
            int angka1 = 0;
            int angka2 = 1;
            int angka3 = 1;

            Console.WriteLine("\n--Fibonacci--");
            Console.Write("Masukkan jumlah bilangan fibonacci : ");
            int fibon = int.Parse(Console.ReadLine());
            for (int i = 0; i < fibon; i++)
            {
                angka3 = angka2 + angka1;
                angka1 = angka2;
                angka2 = angka3;
                if (angka1 % 2 == 0 && angka1 <= fibon)
                {
                    Console.Write(angka1 + ",");
                    Console.Write("Jumlah ada : " + angka1);
                }
            }
            Console.WriteLine();
        }
    }
}
