﻿using System;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Test_Logic1
{
	internal class Program
	{
		static void Main(string[] args)
		{
			SortCharacter();
			PSBB();

			Console.ReadKey();
        }
        static void SortCharacter()
        {
            string vokal = "aiueoAIUEO";
            string konsonan = "bcdfghjklmnpqrstvwxyzBCDFGHJKLMNPQRSTVWXYZ";

            Console.Write("Input one line of words (S) = "); // simple case
            string input = Console.ReadLine().ToLower();
            int x;
            int nilai = 0;

            Console.Write("\n Vowel Characters = ");
            string[] arrayInput1 = new string[input.Length];
            for (x = 0; x < input.Length; x++)
            {
                nilai = vokal.IndexOf(input[x]); 
                if (nilai > -1)
                {
                    arrayInput1[x] = input[x].ToString(); 
                }
            }
            Array.Sort(arrayInput1);
            foreach (string hasil in arrayInput1)
            {
                Console.Write(hasil);
            }
            Console.WriteLine();

            Console.Write("\n Consonant Characters = ");
            string[] arrayInput2 = new string[input.Length];
            for (x = 0; x < input.Length; x++)
            {
                nilai = konsonan.IndexOf(input[x]);
                if (nilai > -1)
                {
                    arrayInput2[x] = input[x].ToString();
                }
            }
            Array.Sort(arrayInput2);
            Array.Reverse(arrayInput2);
            foreach (string hasil in arrayInput2)
            {
                Console.Write(hasil);
            }
            Console.WriteLine();
        }

        static void PSBB()
		{
            Console.WriteLine("Input the number of families: ");
            int amount = int.Parse(Console.ReadLine());
            Console.WriteLine("Input the number of members in the family: ");
			int[] num = Array.ConvertAll(Console.ReadLine().Split(" "), int.Parse);

            if (amount != num.Length)
			{
                Console.WriteLine("Input must be equal with count of family");
			}
			else
			{
                List<int> result = new List<int>();
                for (int i = 0; i < amount; i++)
                {
                    result.Add(num[i]);
                }
                double sumpeople = result.Sum();
                double a = sumpeople / 4;
                double b = Math.Ceiling(a);
                Console.WriteLine("Minimum bus required is : " + b);
            }
        }
           

    }
}
