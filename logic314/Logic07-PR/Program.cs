﻿using System;
using System.ComponentModel;
using System.Linq;

namespace Logic07_PR
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //Console.WriteLine("Hello World!");
            //faktorial();
            //Sos();
            //soalNo4();
            //meminjamBuku();
            //hurufVokalKonsonan();
            starCase();
            //lilinKue();
            //namaBintang();
            //soalNo11();
            Console.ReadLine();
        }

        static void soalNo11()
        {
            Console.WriteLine("");
            Console.Write("Masukkan data angka dengan pemisah (spasi) : ");
            int[] angka = Array.ConvertAll(Console.ReadLine().Split(" "), int.Parse);
            Console.Write("Jumlah Rotasi");
            int rotasi = int.Parse(Console.ReadLine());
            int rot = 0;
            for (int i = 0; i < rotasi; i++)
            {
                for (int j = 0; j < angka.Length; j++)
                {
                    if (j < 1)
                    {
                        rot = angka[j];
                        angka[j] = angka[j + 1];

                    }
                    else if (j >= 1 && j < angka.Length - 1)
                    {
                        angka[j] = angka[j + 1];
                    }
                    else
                    {
                        angka[angka.Length - 1] = rot;
                    }
                }
            }
            Console.Write("\t");
            for (int i = 0; i < angka.Length; i++)
            {
                Console.Write(angka[i] + " ");
            }
            //cara cepat
            // Console.Write(string.Join(" ", angka);
        }

        static void soalNo7()
        {
            int uang = 0;
            int totalHargaMenu = 0;
            int bisaDiMakan = 0;
            int totalBayar = 0;
            int sisa = 0;

            Console.Write("Masukkan total menu = ");
            int totalMenu = int.Parse(Console.ReadLine());
            Console.Write("Masukkan index makanan alergi = ");
            int indexMakananAlergi = int.Parse(Console.ReadLine());
        ulang:
            Console.Write("Masukkan harga menu = ");
            int[] hargaMenu = Array.ConvertAll(Console.ReadLine().Split(","), int.Parse);

            if (hargaMenu.Length > totalMenu)
            {
                Console.WriteLine("Index inputan berlebih, ulang lagi!");
                goto ulang;
            }
            else
            {
                Console.Write("Masukkan uang Anda : ");
                uang = int.Parse(Console.ReadLine());

                for (int i = 0; i < hargaMenu.Length; i++)
                {
                    totalHargaMenu += hargaMenu[i];
                }
                bisaDiMakan = totalHargaMenu - hargaMenu[indexMakananAlergi];
                totalBayar = bisaDiMakan / 2;
                sisa = uang - totalBayar;

                if (sisa > 0)
                {
                    Console.WriteLine($"Anda harus membayar = " + totalBayar.ToString("Rp. #,##"));
                    Console.WriteLine("Sisa uang anda = " + sisa.ToString("Rp. #,##"));
                }
                else if (sisa == 0)
                {
                    Console.WriteLine("Uang anda pas tidak ada kembalian");
                }
                else
                {
                    Console.WriteLine("Uang anda kurang " + sisa.ToString("Rp. #,##"));
                }
            }
        }

        static void soalNo4()
        {
            Console.Write("Masukkan tanggal mulai (dd/mm/yyyy) : ");
            DateTime dateStart = DateTime.Parse(Console.ReadLine());
            Console.Write("Masukkan berapa hari lagi akan ujian : ");
            int ujian = int.Parse(Console.ReadLine());
            Console.Write("Tanggal hari libur : ");
            int[] dateLibur = Array.ConvertAll(Console.ReadLine().Split(","), int.Parse);

            DateTime dateSelesai = dateStart;
            for (int i = 0; i < ujian; i++)
            {
                if (i > 0)
                {
                    dateSelesai = dateSelesai.AddDays(1);
                }
                if (dateSelesai.DayOfWeek == DayOfWeek.Saturday)
                {
                    dateSelesai = dateSelesai.AddDays(2);
                }
                for (int j = 0; j < dateLibur.Length; j++)
                {
                    if (dateSelesai.Day == dateLibur[j])
                    {
                        dateSelesai = dateSelesai.AddDays(1);
                        if (dateSelesai.DayOfWeek == DayOfWeek.Saturday)
                        {
                            dateSelesai = dateSelesai.AddDays(2);
                        }
                    }
                }
            }
            DateTime dateUjian = dateSelesai.AddDays(1);
            if (dateUjian.DayOfWeek == DayOfWeek.Saturday)
            {
                dateUjian=dateUjian.AddDays(2);
            }
            Console.WriteLine("\tKelas akan ujian pada tanggal : " + dateUjian.ToString("dddd, dd/MMMM/yyyy"));
        }

        static void namaBintang()
        {
            // for pertama untuk baris, for kedua baru kolom
            Console.WriteLine("\n---Nama ditengah bintang---");
            Console.Write("Masukkan nama : ");
            string kalimat = Console.ReadLine().Replace(" ",""); //cara menghapus spasi atau icon yang lain menggunakan replace
            string[] kalimatBintang = kalimat.Split(" ");

            foreach (String s in kalimatBintang)
            {
                char[] karakter = s.ToCharArray();
                for (int i = 0; i < karakter.Length; i++)
                {
                    for (int j = 0; j < karakter.Length; j++)
                    {
                        Console.Write("*");
                    }
                    Console.WriteLine(karakter[i]);
                }
                Console.WriteLine(" ");
            }
            for(int s = 0; s < kalimat.Length; s++)
            {
                Console.WriteLine("***" + kalimat[s] + "***");
                
                //char[] karakter = s.ToCharArray();
                //for (int a = 0; a < karakter.Length; a++)
                //{

                //    //Console.Write("*");
                //    if (a <= karakter.Length)
                //    {
                //        Console.Write("***" + karakter[a] + "***");
                //    }
                //    //Console.Write("* ");
                //    Console.WriteLine("\n");
                //}
            }
        }
        static void lilinKue()
        {
            Console.WriteLine("---Tiup Kue Lilin---");
            Console.Write("Input jumlah lilin (pakai spasi) : ");
            string lilin = Console.ReadLine();
            //int[] angkaArray = Array.ConvertAll(Console.ReadLine().Split(" "), int.Parse);
            string[] jumlahLilin = lilin.Split(" ");

            for(int i = 1; i < jumlahLilin.Length; i++)
            {
                if( i == 3)
                {

                }
                else
                {

                }
            }

            //for (int i = 1; i < angkaArray.Length; i++)
            //{
            //    if (angkaArray <= 3)
            //    {
            //        Console.Write(angkaArray[i]);
            //    }
            //}

            //int[] namaArray = { 34, 35, 67 };
            //int sum = 0;
            //foreach (int n in namaArray)
            //{
            //    sum += n;
            //}
            //Console.WriteLine(sum);

            //Console.WriteLine("");

            //foreach (int angka in angkaArray)
            //{
            //    if (angka == 3)
            //    {

            //    }else if (angka == 2)
            //    {

            //    }else if(angka == 1)
            //    {

            //    }
            //    else
            //    {
            //        Console.WriteLine($"Tingkatan lilin hanya sampai lilin tertinggi = {angka}");
            //    }
            //}
        }

        static void starCase()
        {
            Console.WriteLine("Membuat segitiga siku-siku");
            Console.Write("Masukkan angka inputan : ");
            int input = int.Parse(Console.ReadLine());

            Console.WriteLine("Segitiga Siku Kiri");
            for (int i = input; i > 0; i--)
            {
                for (int j = 1; j <= input; j++)
                {
                    if( j >= i)
                    {
                        Console.Write("*");
                    }
                    else
                    {
                        Console.Write(" ");
                    }
                }
                Console.WriteLine(" ");
            }

            Console.WriteLine("Segitiga Siku Kanan Kebalik");
            for (int i = 0; i < input; i++)
            {
                //for (int j = i; j < input; j++) cara disamping membuat segitiga siku kebalik
                for(int j = i ; j < input; j++)
                {
                    Console.Write("*");
                }
                Console.WriteLine(" ");
            }

            Console.WriteLine("Segitiga Siku Kanan");
            for (int i = 0; i < input; i++)
            {
                for (int j = 0; j <= i; j++)
                {
                    Console.Write("*");
                }
                Console.WriteLine(" ");
            }
        }

        static void hurufVokalKonsonan()
        {
            Console.WriteLine("Menghitung huruf vokal dan konsonan");
            Console.Write("Masukkan kata : ");
            string huruf = Console.ReadLine().ToLower();
            string charVokal = "", charKonsonan = "";

            foreach (char c in huruf)
            {
                if((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z'))
                {
                    if (c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u')
                    {
                        charVokal += c;
                    }
                    else
                    {
                        charKonsonan += c;
                    }
                }
            }
            Console.WriteLine("\nHuruf Vokal : " + charVokal );
            Console.WriteLine("Huruf Vokal : " + charVokal.Length);
            Console.WriteLine("\nHuruf Konsonan : " + charKonsonan);
            Console.WriteLine("Huruf Konsonan : " + charKonsonan.Length);
        }

        static void meminjamBuku()
        {
            Console.WriteLine("---PR Meminjam Buku---");
            Console.Write("\nTanggal pertama meminjam buku (dd/mm/yyyy) : ");
            string tanggalPertama = Console.ReadLine();
            Console.Write("\nTanggal mengembalikan buku (dd/mm/yyyy) : ");
            string tanggalMengembalikan = Console.ReadLine();
            DateTime tanggalPertamaMinjam = DateTime.Parse(tanggalPertama);
            DateTime mengembalikanTanggal = DateTime.Parse(tanggalMengembalikan);

            DateTime date2 = DateTime.Now;//jika ingin tanggal pengembalian nya hari ini tinggal kita panggil date2 dirumus

            TimeSpan span = mengembalikanTanggal - tanggalPertamaMinjam;

            int denda = span.Days * 500;
            
            if (span.Days <= 0)
            {
                Console.WriteLine("Masukkan tanggal awal pinjam dengan benar!");
            }
            else if (span.Days <= 3)
            {
                Console.WriteLine("Terima kasih telah mengembalikkan buku sesuai dengan maksimal pinjam 3 hari!");
                Console.WriteLine($"Total hari : {span.Days}");
            }
            else
            {
                Console.WriteLine($"Total hari : {span.Days}");
                Console.WriteLine($"Anda mengembalikkan buku lebih dari 3 hari dengan total pinjam {span.Days} hari x denda Rp.500/hari = {denda}");
            }

        }

        static void Sos()
        {
            //sample input a = SOSSPSSQSSOR sample output sinyal yg benar SOSSOSSOSSOSSOSSOS, sinyal yang diterima output variabel {a} total sinyal salah 3
            //kebalikan ==  sama && yaitu tidak sama dengan dan ||(or)

            Console.WriteLine("---PR Signal SOS---");
            Console.Write("\nMasukkan Sinyal SOS : ");
            char[] sinyal = Console.ReadLine().ToUpper().ToCharArray();

            int pembanding = 0;
            int countB = 0;
            string tmp = "";

            if (sinyal.Length % 3 != 0)
                {
                    Console.WriteLine("Invalid!");
                }
                else
                {
                    for (int i = 0; i < sinyal.Length; i += 3)
                    {
                        //if (sinyal[i] == 'S' && sinyal[i + 1] == 'O' && sinyal[i + 2] == 'S')
                        //{
                    
                        //}
                        //else
                        if(sinyal[i] != 'S' || sinyal[i + 1] != 'O' || sinyal[i + 2] != 'S')
                        {
                            pembanding++;
                            tmp += "SOS";
                        }
                        else
                        {
                            tmp += sinyal[i].ToString() + sinyal[i+1].ToString() + sinyal[i+2].ToString();
                            countB++;
                        }
                    }
                    Console.WriteLine($"Total sinyal salah : {pembanding}");
                    Console.WriteLine($"Total sinyal benar : {countB}");
                    Console.WriteLine($"Sinyal yang diterima : {String.Join("", sinyal)}");
                    Console.WriteLine($"Sinyal yang benar : {tmp}");
                }
        }

        static void faktorial()
        {
            Console.WriteLine("---PR Faktorial---");
            Console.Write("Jumlah anak : ");
            int anak = int.Parse(Console.ReadLine());

            Console.Write($"{anak}! = ");
            int hasil = 1;
            string angka = "";
            
            //cara pertama
            for (int i = anak; i >= 1; i--)
            {
                hasil *= i;
                angka += angka == "" ? i.ToString() : " * " + i.ToString(); 
            }

            //cara kedua
            //for (int i = anak; i >= 1; i--)
            //{
            //    hasil *= i;
            //    angka += angka == "" ? i.ToString() : " * " + i.ToString();
            //    Console.Write(i);
            //    if (i > 1)
            //    {
            //        Console.Write(" * ");
            //    }
            //    else
            //    {
            //        Console.Write($" = {hasil}");
            //    }
            //}
            Console.WriteLine($"{angka} = {hasil}");
            Console.WriteLine($"\nAda {hasil} cara");
        }
    }
}
