﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Logic06
{
    internal class Mobil
    {
        public double kecepatan;
        public double bensin;
        public double posisi;
        public string nama;
        private string platno;

        public Mobil() { }
        public Mobil(String platno)
        {
            this.platno = platno;
        }
        public void percepat()
        {
            this.kecepatan += 10;
            this.bensin -= 5;
        }
        public void maju()
        {
            this.posisi += this.kecepatan;
            this.bensin -= 2;
        }
        public void isiBensin(double liter)
        {
            this.bensin += liter;
        }
        public String getPlatNo()
        {
            return platno;
        }
    }

}
