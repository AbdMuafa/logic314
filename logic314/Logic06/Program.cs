﻿using System;
using System.Collections.Generic;

namespace Logic06
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //list();
            //contohClass();
            //listClass();
            addList();
            Console.ReadKey();
        }
        static void contohClass()
        {

            Mobil mobil = new Mobil("RI SATU");

            mobil.percepat();
            mobil.maju();
            mobil.isiBensin(12);
            string platNo = mobil.getPlatNo();

            Console.WriteLine($"Plat Nomor : {platNo}");
            Console.WriteLine($"Sisa Bensin: {mobil.bensin}");
            Console.WriteLine($"Kecepatan  : {mobil.kecepatan}");
            Console.WriteLine($"Posisi     : {mobil.posisi}");
        }
        static void list()
        {
            Console.WriteLine("---List---");
            List<String> strings = new List<String>()
            {
                "Astika",
                "Marchelino",
                "Alwi",
                "Toni"
            };
            Console.WriteLine(string.Join(", ", strings));
        }
        static void listClass()
        {
            List<User> users = new List<User>()
            {
                new User() {Name = "Isni", Age = 22},
                new User() {Name = "Astika", Age =23 },
                new User() {Name = "Alfi", Age = 23 }
            };

            for (int i = 0; i < users.Count; i++)
            {
                Console.WriteLine(users[i].Name + " sudah berumur " + users[i].Age + " tahun");
            }
        }
        static void addList()
        {
            List<User> users = new List<User>()
            {
                new User() {Name = "Isni", Age = 22},
                new User() {Name = "Astika", Age =23 },
                new User() {Name = "Alfi", Age = 23 }
            };

            users.Add(new User() { Name = "Marchel", Age = 22 });

            List<String> strings = new List<String>();
            strings.Add("WAW");
            strings.Add("WOW");
            for (int i = 0; i < users.Count; i++)
            {
                Console.WriteLine(users[i].Name + " sudah berumur " + users[i].Age + " tahun");
            }
            for (int i = 0; i < strings.Count; i++)
            {
                Console.WriteLine(strings[i]);
            }
        }
    }

}
