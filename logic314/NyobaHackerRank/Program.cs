﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;

namespace Logic15
{
    class Program
    {
        static void Main(string[] args)
        {
            //Console.WriteLine("Hello World!");
            //strongPassword();
            //reducedString();
            //bilPrima();
            //inputPlusMinus();
            //soalNo7SimLogic();
            //soalNo2SimLogic();
            //soalNo3SimLogic();
            //soalNo4SimLogic();
            soalNomor5();
            //SoalKisiKisi_1();
            //soalKisiKisi_2();
            //soalNo6();
            //rekursif();
            //rekursifAsc();
            //soalNo9SimLogic();
            //soalNo1SimLogic();

            Console.ReadKey();
        }

        static void rekursif()
        {
            //overload
            Console.Write("Masukkan input : ");
            int angka = int.Parse(Console.ReadLine());
            //panggil fungsi
            int start = 1;
            perulangan(angka, start);
        }
        static int perulangan(int angka)
        {
            if (angka == 0)
            {
                return angka;
            }
            Console.WriteLine(angka);
            return perulangan(angka - 1);

            // kebalikan dari atas atau desc
        }

        //static void rekursifAsc()
        //{
        //    Console.Write("Masukkan input : ");
        //    int angka = int.Parse(Console.ReadLine());
        //    //panggil fungsi
        //    perulanganAsc(angka,start);
        //}

        static int perulangan(int angka, int start) { 
            if (start == angka)
            {
                Console.WriteLine(start);
                return start;
            }
            Console.WriteLine(start);
            return perulangan(angka, start + 1);
        }

        static void soalNo6()
        {
            Console.WriteLine("--Soal No 6--");
            Console.Write("Masukkan kalimat : ");
            string kalimat = Console.ReadLine().ToLower();

            string alfabet = "abcdefghijklmnopqrstuvwxyz";
            bool isPangram = true;
            foreach (char item in alfabet)
            {
                if (!kalimat.Contains(item))
                {
                    isPangram = false;
                    break;
                }
            }
            if (isPangram)
            {
                Console.WriteLine("Kalimat ini Pangram");
            }
            else
            {
                Console.WriteLine("Kalimat ini bukan Pangram");
            }
        }

        static void soalNo4SimLogic()
        {
            int jumlah, jumlahBajuLaki = 0, jumlahBajuWanita = 0, jumlahBajuAnak = 0, jumlahBajuBayi = 0, total = 0, jumlahWanita = 0;
            Console.WriteLine("\tPilihlah baju yang akan di bagi:");
            Console.WriteLine("\t1. Laki Dewasa");
            Console.WriteLine("\t2. Wanita Dewasa");
            Console.WriteLine("\t3. Anak-anak");
            Console.WriteLine("\t4. Bayi");
        lanjut:
            Console.Write("\tPilih kategori baju: ");
            int pilih = int.Parse(Console.ReadLine());
            char ulang = ' ';

            switch (pilih)
            {
                case 1:
                    Console.Write("\tJumlah baju Laki-laki dewasa: ");
                    jumlah = int.Parse(Console.ReadLine());
                    jumlahBajuLaki += jumlah;
                    //total += jumlahBajuLaki;
                    Console.Write("\tApakah kamu akan input lagi?(y/n): ");
                    ulang = char.Parse(Console.ReadLine());
                    if (ulang == 'y')
                        goto lanjut;
                    break;
                case 2:
                    Console.Write("\tJumlah baju wanita dewasa: ");
                    jumlahWanita = int.Parse(Console.ReadLine());
                    jumlahBajuWanita += jumlahWanita * 2;
                    //total += jumlahBajuWanita;
                    Console.Write("\tApakah kamu akan input lagi?(y/n): ");
                    ulang = char.Parse(Console.ReadLine());
                    if (ulang == 'y')
                        goto lanjut;
                    break;
                case 3:
                    Console.Write("\tJumlah baju anak-anak: ");
                    jumlah = int.Parse(Console.ReadLine());
                    jumlahBajuAnak += jumlah * 3;
                    //total += jumlahBajuAnak;
                    Console.Write("\tApakah kamu akan input lagi?(y/n): ");
                    ulang = char.Parse(Console.ReadLine());
                    if (ulang == 'y')
                        goto lanjut;
                    break;
                case 4:
                    Console.Write("\tJumlah baju bayi: ");
                    jumlah = int.Parse(Console.ReadLine());
                    jumlahBajuBayi += jumlah * 5;
                    //total += jumlahBajuBayi;
                    Console.Write("\tApakah kamu akan input lagi?(y/n): ");
                    ulang = char.Parse(Console.ReadLine());
                    if (ulang == 'y')
                        goto lanjut;
                    break;
                default:
                    Console.WriteLine("\tTidak ada pilihan");
                    goto lanjut;
            }
            total = jumlahBajuAnak + jumlahBajuBayi + jumlahBajuLaki + jumlahBajuWanita;

            if (total % 2 == 1 && total > 10)
                total += jumlahWanita;

            Console.WriteLine("\tTotal baju: " + total);


        }

        static void kembali()
        {
            Console.WriteLine("\nApakah ingin input lagi? (y/n) ");
            string menuInput = Console.ReadLine().ToLower();
            if (menuInput == "y")
            {
                soalNo4SimLogic();
                //Console.Clear();
            }
        }

        static void soalNomor5()
        {
            Console.WriteLine("\t===== Pembulatan Nilai =====");
            Console.Write("\tMasukkan nilai: ");
            int nilaiAsal = int.Parse(Console.ReadLine());

            double nilaiakhir = 0, nilaiUbah = 0, temp;

            temp = Math.Round((nilaiAsal / 5d), MidpointRounding.ToPositiveInfinity);
            nilaiUbah = temp * 5;

            if (nilaiUbah - nilaiAsal < 3)
                nilaiakhir = nilaiUbah;
            else
                nilaiakhir = nilaiAsal;
            Console.WriteLine("Nilai akhir pembulatan adalah: " + nilaiakhir);
        }

        static void SoalKisiKisi_1()
        {
            //List<int> list = new List<int>() 
            //{1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

            List<int> list = new List<int>();
            list.Add(1);
            list.Add(2);
            list.Add(3);
            list.Add(4);
            list.Add(5);
            list.Add(6);
            list.Add(7);
            list.Add(8);
            list.Add(9);
            list.Add(10);

            for (int i = 0; i < list.Count; i++)
            {
                if (list[i] % 2 == 0)
                {
                    list.RemoveAt(i);
                }
                Console.WriteLine(String.Join(", ", list));
            }
        }

        static void soalKisiKisi_2()
        {
            Console.WriteLine("--Menampilkan bilangan prima terkecil dari inputan--");
            Console.Write("Masukkan bilangan : ");
            int bilangan = int.Parse(Console.ReadLine());

            int tampung = 0;

            bool prima = true;

            if (bilangan >= 2)
            {
                //for (int i = 2; i <= bilangan; i++) // untuk print all bilangan prima
                //{
                for (int j = 2; j < bilangan; j++)
                {
                    if (bilangan % j == 0)
                    {
                        tampung = j;
                        prima = false;
                        //jika breaknya di hapus maka akan tampil pembagi terbesarnya
                        //break; // jika di komentar akan memunculkan pembagi terkecil
                    }
                }
                if (prima)
                {
                    Console.WriteLine($"1 ");
                }
                else
                {
                    Console.WriteLine($"{tampung}");
                }
                prima = true;
                //}
            }
            else
            {
                Console.WriteLine("Inputan bukan bilangan prima");
            }
        }

        static void soalNo9SimLogic()
        {
            Console.WriteLine("--Soal 9--");
            Console.Write("Masukkan beli pulsa = ");
            int pulsa = int.Parse(Console.ReadLine());
            int poin1 = 0, poin2 = 0;

            if (pulsa > 0 && pulsa <= 10000)
            {
                Console.WriteLine($"0 = 0 Point");
            }
            else if (pulsa <= 30000)
            {
                poin1 = (pulsa - 10000) / 1000;
                Console.WriteLine($"0 + {poin1} = {poin1} Point");
            }
            else
            {
                poin1 = 20000 / 1000;
                poin2 = ((pulsa - 30000) / 1000) * 2;
                Console.WriteLine($"0 + {poin1} + {poin2} = {poin1 + poin2} Point");
            }
        }
        static void soalNo1SimLogic()
        {
            Console.Write("Masukkan kata : ");
            string huruf = Console.ReadLine();
            string charVokal = "";


            foreach (char c in huruf)
            {
                if ((c >= 'A' && c <= 'Z'))
                {
                    charVokal += c;
                }
            }

            Console.WriteLine("\nHuruf Vokal : " + charVokal);
            Console.WriteLine("Huruf Vokal : " + charVokal.Length);

            //Cara 2

            int count = 0;
            foreach (char item in huruf)
            {
                if (char.IsUpper(item))
                {
                    count++;
                }
            }
            Console.WriteLine("Output : " + count);
        }

        static void soalNo3SimLogic()
        {
            Console.WriteLine("--keranjang buah--");
            int hasil = 0;
            int keranjang1 = 0, keranjang2 = 3, keranjang3 = 10;
            Console.Write("Masukkan keranjang 1 : ");
            int ker1 = int.Parse(Console.ReadLine());
            Console.Write("Masukkan keranjang 2 : ");
            int ker2 = int.Parse(Console.ReadLine());
            Console.Write("Masukkan keranjang 3 : ");
            int ker3 = int.Parse(Console.ReadLine());
            Console.Write("Pilih keranjang yang ingin dibawa ke pasar ? ");
            int keranjang = int.Parse(Console.ReadLine());

            if (keranjang == 1)
            {
                hasil = ker2 + ker3;
            }
            else if (keranjang == 2)
            {
                hasil = ker1 + ker3;
            }
            else if (keranjang == 3)
            {
                hasil = ker1 + ker2;
            }
            else
            {
                Console.WriteLine("Masukkan inputan angka!");
            }
            Console.WriteLine($"Sisa Buah {hasil}");
        }

        static void soalNo2SimLogic()
        {
            Console.WriteLine("--invoice penjualan--");
            Console.WriteLine("Input : ");
            Console.Write("Start = ");
            int start = int.Parse(Console.ReadLine());
            Console.Write("End = ");
            int end = int.Parse(Console.ReadLine());
            string inisial = "XA";
            //Console.WriteLine("Inisial Perusahaan (XSIS Academy - XA) = ");
            //string inisial = Console.ReadLine();
            DateTime tgl = DateTime.Now;

            for (int i = start; i <= end; i++)
            {
                Console.WriteLine($"{inisial}-{tgl.ToString("dd/MM/yyyy")}-{i.ToString().PadLeft(5, '0')}");
            }
        }

        static void soalNo7SimLogic()
        {
            int angka1 = 0, angka2 = 1, angka3 = 1;
            Console.Write("Masukkan himpunan : ");
            int himp = int.Parse(Console.ReadLine());

            for (int i = 0; i < himp; i++)
            {
                angka3 = angka2 + angka1;
                angka1 = angka2;
                angka2 = angka3;
                Console.Write($"{angka1}, ");

            }
            Console.WriteLine();
            int totalgenap = 0;
            for (int i = 1; i <= himp; i++)
            {
                if (himp % 2 == 0)
                {
                    totalgenap += himp;
                    Console.Write($"{i * 2}, ");
                }
            }
            Console.WriteLine("\ntotal genap : " + totalgenap);

            for (int j = 1; j <= himp; j++)
            {
                if (j % 2 != 0)
                {
                    Console.Write($"{j}, ");
                }
            }

        }

        static void bilPrima()
        {
            Console.WriteLine("Bilangan Prima");
            Console.Write("Masukkan jumlah bilangannya : ");
            int bil = int.Parse(Console.ReadLine());

            bool prima = true;

            for (int i = 2; i <= bil; i++)
            {
                for (int j = 2; j < i; j++)
                {
                    if (i % j == 0)
                    {
                        prima = false;
                        break;
                    }
                }
                if (prima)
                    Console.Write($"{i}, ");
                prima = true;
            }

        }

        public static void inputPlusMinus()
        {
            Console.Write("Masukkan angka : ");
            int n = Convert.ToInt32(Console.ReadLine().Trim());
            Console.Write("Masukkan list angka (menggunakan spasi ): ");
            //trim untuk memotong whitespace
            List<int> arr = Console.ReadLine().TrimEnd().Split(' ').ToList().Select(arrTemp => Convert.ToInt32(arrTemp)).ToList();

            plusMinus(arr);
        }

        public static void plusMinus(List<int> arr)
        {
            int a = 0, b = 0, c = 0;
            for (int i = 0; i < arr.Count; i++)
            {
                int x = arr[i];
                if (x > 0)
                {
                    a++;
                }
                else if (x < 0)
                {
                    b++;
                }
                else
                {
                    c++;
                }
            }
            decimal positive = Convert.ToDecimal(a) / arr.Count;
            decimal negative = Convert.ToDecimal(b) / arr.Count;
            decimal zero = Convert.ToDecimal(c) / arr.Count;
            positive = Math.Round(positive, 6);
            negative = Math.Round(negative, 6);
            zero = Math.Round(zero, 6);

            Console.WriteLine(positive.ToString("0.000000"));
            Console.WriteLine(negative.ToString("N6"));
            Console.WriteLine(string.Format("{0:N6}", zero));
        }

        public static void strongPassword()
        {
            Console.Write("Masukkan jumlah karakter : ");
            int n = Convert.ToInt32(Console.ReadLine().Trim());

            Console.Write("Masukkan Password : ");
            string password = Console.ReadLine();

            int answer = minimumNumber(n, password);

            Console.WriteLine(answer);

            //textWriter.Flush();
            //textWriter.Close();
        }

        public static int minimumNumber(int n, string password)
        {
            int angka = 1, hurufKecil = 1, hurufBesar = 1, karSpesial = 1;
            int hasil = 0;
            for (int i = 0; i < n; i++)
            {
                if (!char.IsLetterOrDigit(password[i]) && karSpesial > 0)
                {
                    karSpesial = 0;
                }
                else if (char.IsUpper(password[i]) && hurufBesar > 0)
                {
                    hurufBesar = 0;
                }
                else if (char.IsLower(password[i]) && hurufKecil > 0)
                {
                    hurufKecil = 0;
                }
                else if (char.IsNumber(password[i]) && angka > 0)
                {
                    angka = 0;
                }

                if (angka + hurufKecil + hurufBesar + karSpesial == 0)
                {
                    return Math.Max(6 - n, 0);
                }
            }
            hasil = Math.Max(6 - n, angka + hurufKecil + hurufBesar + karSpesial);
            //mengembalikan nilai lebih besar Math.Max
            return hasil;
        }

        public static void reducedString()
        {
            Console.WriteLine("Reduced String");
            Console.Write("Masukkan kata/kalimat : ");
            string s = Console.ReadLine();
            string result = superReducedString(s);

            Console.WriteLine(result);
        }

        public static string superReducedString(string s)
        {
            string d, e;
            for (int i = 0; i < s.Length - 1; i++)
            {
                d = s.Substring(i, 1);
                e = s.Substring(i + 1, 1);
                if (e == d)
                {
                    s = s.Remove(i, 2);
                    if (i == 0) i = i - 1;
                    else i = i - 2;
                }
            }
            if (s.Length < 1)
            {
                s = "Empty String";
            }
            return s;
        }
    }
}
