﻿using System;

namespace Logic05
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //contain();
            //convertArray();
            padLeft();

            Console.ReadKey();
        }

        static void convertArray()
        {
            Console.WriteLine("---Convert Array All---");
            Console.Write("Masukkan angka array (pakai koma , ): ");
            int[] angkaArray = Array.ConvertAll(Console.ReadLine().Split(","),int.Parse);

            Console.WriteLine(String.Join("|", angkaArray));
            
        }

        static void padLeft()
        {
            Console.WriteLine("---Pad Left---");
            Console.Write("Masukkan input : ");
            int input = int.Parse(Console.ReadLine());
            Console.Write("Masukkan panjang karakter : ");
            int panjang = int.Parse(Console.ReadLine());
            Console.Write("Masukkan char : ");
            char chars = char.Parse(Console.ReadLine());

            Console.WriteLine($"Hasil PadLeft : {input.ToString().PadLeft(panjang,chars)}");
        }

        static void contain()
        {
            Console.WriteLine("---Contain---");
            Console.Write("Masukkan kalimat :");
            string kalimat = Console.ReadLine();

            Console.Write("Masukkan contain : ");
            string contain = Console.ReadLine();

            if (kalimat.Contains(contain))
            {
                Console.WriteLine($"Kalimat dari ({kalimat}) ini mengandung {contain}");
            }
            else
            {
                Console.WriteLine($"Kalimat dari ({kalimat}) ini tidak mengandung {contain}");

            }
        }
    }
}
