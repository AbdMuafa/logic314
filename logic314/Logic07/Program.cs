﻿using System;
using System.Collections.Generic;

namespace Logic07
{
    class Mamalia
    {
        public virtual void pindah()
        {
            Console.WriteLine("Lari...");
        }
    }
    class kucing : Mamalia //kucing masuk ke class mamalia atau turunan
    {

    }
    class Paus : Mamalia //override untuk mengubah 
    {
        public override void pindah()
        {
            Console.WriteLine("Berenang...");
        }
    }
    public class Program
    {
        static List<User> listUser = new List<User>();
        static void Main(string[] args)
        {
            //listUsers();
            //dateTime();
            bool ulangi = true;
            while (ulangi)
            {
                insertUser();
                kembali();
            }
            ulangi = false;

            //stringDateTime();

            //timeSpan();

            //overRidding();
            Console.ReadKey();
        }

        static void overRidding()
        {
            Paus paus = new Paus();
            paus.pindah();

            kucing kucing = new kucing();
            kucing.pindah();
        }

        static void ClassInheritance() // contoh inheritance dari class dan object oop
        {
            // mobilType dibawah ini adalah object / atau cara memanggil object di child class
            TypeMobil mobilType = new TypeMobil();
            mobilType.kecepatan = 100;
            mobilType.posisi = 50; // ini untuk input ke 
            mobilType.Civic();// ini untuk ngeprint
        }

        static void timeSpan()
        {
            Console.WriteLine("\n------Time Span------");
            
            DateTime date1 = new DateTime(2023, 1, 1, 1, 0, 0);
            DateTime date2 = DateTime.Now;
            
            TimeSpan span = date2 - date1;

            Console.WriteLine($"Total hari : {span.Days}");
            Console.WriteLine($"Total hari : {span.TotalDays}");
            Console.WriteLine($"Interval hari : {span.Hours}");
            Console.WriteLine($"Total jam : {span.TotalHours}");
            Console.WriteLine($"Total menit : {span.TotalMinutes}");
            Console.WriteLine($"Total detik : {span.TotalSeconds}");

        }

        static void stringDateTime()
        {
            Console.Write("");
            Console.WriteLine("\n------String Date Time------");
            Console.Write("Masukkan tanggal (dd/mm/yyyy) : ");
            string tanggal = Console.ReadLine();

            DateTime tanggalDateTime = DateTime.Parse(tanggal);
            
            //Console.WriteLine($"Tanggal : {tanggal}");
            Console.WriteLine($"tanggal : {tanggalDateTime.Day}");
            Console.WriteLine($"Bulan : {tanggalDateTime.Month}");
            Console.WriteLine($"Tahun : {tanggalDateTime.Year}");
            Console.WriteLine($"Day Of Week ke -  : {(int)tanggalDateTime.DayOfWeek }");
            Console.WriteLine($"Day Of Week ke -  : {tanggalDateTime.DayOfWeek }");
        }

        static void dateTime()
        {
            Console.WriteLine("\n------Date Time------");
            DateTime dt1 = new DateTime();
            Console.WriteLine(dt1);
            DateTime dt2 = DateTime.Now;
            Console.WriteLine(dt2);
            DateTime dt3 = DateTime.Now.Date;
            Console.WriteLine(dt3);
            DateTime dt4 = DateTime.UtcNow;
            Console.WriteLine(dt4);
            DateTime dt5 = new DateTime(2023, 03, 09);
            Console.WriteLine(dt5);

            DateTime dt6 = new DateTime(2023, 9, 9, 11, 45, 13);
            Console.WriteLine(dt6);
        }

        static void kembali()
        {
            Console.WriteLine("\nApakah ingin input lagi? (y/n) ");
            string menuInput = Console.ReadLine().ToLower();
            if (menuInput == "y")
            {
                insertUser();
                //Console.Clear();
            }
        }

        static void insertUser()
        {
            Console.WriteLine("Input New User");
            Console.Write("Masukkan nama anda : ");
            string nama = Console.ReadLine();
            Console.Write("Masukkan umur anda : ");
            int umur = int.Parse(Console.ReadLine());
            Console.Write("Masukkan alamat : ");
            string alamat = Console.ReadLine();


            //List<User> listUser = new List<User>()
            //{
            //    //new User(){Nama = nama, Umur = umur, Alamat = alamat},
            //    new User(){Nama = "Firdha", Umur = 24, Alamat="Tangsel"},
            //    new User(){Nama = "Isni", Umur = 22, Alamat="Cimahi"},
            //    new User(){Nama = "Asti", Umur = 23, Alamat="Garut"},
            //    new User(){Nama = "Muafa", Umur = 22, Alamat="Bogor"},
            //    new User(){Nama = "Toni", Umur = 24, Alamat="Garut"},
            //};

            //listUser.Add(new User());

            User user = new User();
            user.Nama = nama;
            user.Umur = umur;
            user.Alamat = alamat;
            listUser.Add(user);
            
            Console.WriteLine("--List User--");
            for (int i = 0; i < listUser.Count; i++)
            {
                Console.WriteLine("Nama : " + listUser[i].Nama + ", berumur : " + listUser[i].Umur + " tahun, " + "alamat  : " + listUser[i].Alamat);
            }
        }

        static void listUsers()
        {
            List<User> listUsers = new List<User>()
            {
                new User(){Nama = "Firdha", Umur = 24, Alamat="Tangsel"},
                new User(){Nama = "Isni", Umur = 22, Alamat="Cimahi"},
                new User(){Nama = "Asti", Umur = 23, Alamat="Garut"},
                new User(){Nama = "Muafa", Umur = 22, Alamat="Bogor"},
                new User(){Nama = "Toni", Umur = 24, Alamat="Garut"},
            };
            Console.WriteLine("--List User--");
            for (int i = 0; i < listUsers.Count; i++)
            {
                Console.WriteLine("Nama : " + listUsers[i].Nama + ", berumur : " + listUsers[i].Umur + " tahun, " + "alamat  : " + listUsers[i].Alamat);
            }
        }
    }
}
