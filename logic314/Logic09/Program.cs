﻿using System;

namespace Logic09
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //soalNo1();
            //soalNo2();
            //soalNo3();
            soalTiga_cara2();
            Console.ReadKey();
        }
        static void soalNo1()
        {
            Console.WriteLine("Soal Nomor 1");
            Console.WriteLine("1. Jarak dari toko ke Customer 1 = 2KM");
            Console.WriteLine("2. Jarak dari Customer 1 ke Customer 2 = 500M");
            Console.WriteLine("3. Jarak dari Customer 2 ke Customer 3 = 1.5KM");
            Console.WriteLine("4. Jarak dari Customer 3 ke Customer 4 = 300M");

            Console.Write("Masukkan Jumlah Customer : ");
            int customer = int.Parse(Console.ReadLine());

            double jarakCustomer1 = 2, jarakCustomer2 = 0.5, jarakCustomer3 = 1.5, jarakCustomer4 = 0.3, bensinPerLiter = 2.5;
            double totalBensin = 0, totalJarak = 0;
            if (customer == 1)
            {
                totalJarak = jarakCustomer1;
                totalBensin = totalJarak % bensinPerLiter == 0 ? (totalJarak / bensinPerLiter) : Math.Round(totalJarak / bensinPerLiter, MidpointRounding.ToPositiveInfinity);
                Console.WriteLine($"Jaram Tempuh : {jarakCustomer1}KM");
                Console.WriteLine($"Bensin : {totalBensin} Liter");
            }
            else if (customer == 2)
            {
                totalJarak = jarakCustomer1 + jarakCustomer2;
                totalBensin = totalJarak % bensinPerLiter == 0 ? (totalJarak / bensinPerLiter) : Math.Round(totalJarak / bensinPerLiter, MidpointRounding.ToPositiveInfinity);
                Console.WriteLine($"Jaram Tempuh : {jarakCustomer1}KM + {jarakCustomer2}KM = {totalJarak}KM");
                Console.WriteLine($"Bensin : {totalBensin} Liter");
            }
            else if (customer == 3)
            {
                totalJarak = jarakCustomer1 + jarakCustomer2 + jarakCustomer3;
                totalBensin = totalJarak % bensinPerLiter == 0 ? (totalJarak / bensinPerLiter) : Math.Round(totalJarak / bensinPerLiter, MidpointRounding.ToPositiveInfinity);
                Console.WriteLine($"Jaram Tempuh : {jarakCustomer1}KM + {jarakCustomer2}KM + {jarakCustomer3}KM = {totalJarak}KM");
                Console.WriteLine($"Bensin : {totalBensin} Liter");
            }
            else if (customer == 4)
            {
                totalJarak = jarakCustomer1 + jarakCustomer2 + jarakCustomer3 + jarakCustomer4;
                totalBensin = totalJarak % bensinPerLiter == 0 ? (totalJarak / bensinPerLiter) : Math.Round(totalJarak / bensinPerLiter, MidpointRounding.ToPositiveInfinity);
                Console.WriteLine($"Jaram Tempuh : {jarakCustomer1}KM + {jarakCustomer2}KM + {jarakCustomer3}KM + {jarakCustomer4}KM = {totalJarak}KM");
                Console.WriteLine($"Bensin : {totalBensin} Liter");
            }
            else
            {
                Console.WriteLine($"Jumlah Customer tidak kurang dari 1 dan tidak lebih dari 4");
            }
        }

    static void soalNo1Cara2()
        {
            Console.Write("Masukkan jumlah customer : ");
            double jumlahCustomer = double.Parse(Console.ReadLine());

            double jarak = 1;
            double bensin = 2.5;
            double total;

            if (jumlahCustomer == 4)
            {
                jarak = 300 * 10 / 1000;
                bensin *= jarak / bensin;
            }
            else if (jumlahCustomer == 3)
            {
                jarak = 1500 / 1000;
                bensin *= jarak;
            }
            else if (jumlahCustomer == 2)
            {
                jarak = 500 * 10 / 1000;
                bensin *= jarak;
            }
            else if (jumlahCustomer == 1)
            {
                jarak = 2000 / 1000;
                bensin /= jarak;
            }
            else
            {
                Console.WriteLine("Masukkan jumlah customer dengan benar!");
            }

            Console.WriteLine("Jarak tempuh = " + jarak);
            Console.WriteLine("Jarak tempuh = 2KM + 5M + 1.5 KM = 4 KM");
            Console.WriteLine("Bensin = " + bensin + " Liter");
        }

        static void soalNo2()
        {
            Console.Write("Masukkan jumlah kue pukis : ");
            int pukis = int.Parse(Console.ReadLine());

            double terigu = 115;
            double gulaPasir = 190;
            double susu = 100;

            Console.WriteLine("Bahan terigu yang dibutuhkan : " + Math.Round((terigu * pukis)/15));
            Console.WriteLine("Bahan gula pasir yang dibutuhkan : " + Math.Round((gulaPasir * pukis)/15));
            Console.WriteLine("Bahan susu yang dibutuhkan : " + Math.Round(( susu * pukis)/15 ));
        }

        static void soalNo3()
        {
            Console.WriteLine("segitiga soal no 3");
            Console.Write("Masukkan jumlah n : ");
            int input = int.Parse(Console.ReadLine());
            Console.Write("Kelipatan : ");
            int kelipatan = int.Parse(Console.ReadLine());

            //dibawah ini cara awal
            for (int i = 1; i < input; i++)
            {
                if (i != 1)
                {
                    Console.WriteLine($"\n{kelipatan}");
                }

                for (int k = i ; k >= 2; k--)
                {
                    Console.Write("\t");
                }
                Console.Write(i * kelipatan + "\t");
                
                Console.WriteLine();
            }
                Console.WriteLine("\n");
            for (int e = 1; e <= input; e++)
            {
                Console.Write(e * kelipatan + "\t");
            }

            //dibawah ini cara benar dan rapih
            //for (int i = 0; i < input; i++)
            //{
            //    int a = 0;
            //    for (int j = 0; j < input; j++)
            //    {
            //        a += 2;
            //        if (j == 0 || i == input-1 || i == j)
            //        {
            //            Console.Write(a.ToString() + " ");
            //        }
            //        else
            //        {
            //            Console.Write("  ");
            //        }
            //    }
            //    Console.WriteLine();
            //}
        }

        static void soalTiga_cara2()
        {
            Console.WriteLine("segitiga soal no 3 cara 2");
            Console.Write("Masukkan jumlah n : ");
            int input = int.Parse(Console.ReadLine());
            Console.Write("Kelipatan : ");
            int kelipatan = int.Parse(Console.ReadLine());
            int hasil = kelipatan;

            for (int y = 0; y < input; y++)
            {
                for (int z = 0; z <= y; z++)
                {
                    if (z == 0)
                    {
                        hasil = kelipatan;
                        Console.Write(hasil + "\t");
                    }
                    else if (z == y && z < input - 1)
                    {
                        hasil += kelipatan * z;
                        Console.Write(hasil);
                    }
                    else if (y == input - 1)
                    {
                        hasil += kelipatan;
                        Console.Write(hasil + "\t");
                    }
                    else
                    {
                        Console.Write("\t");
                    }
                    Console.WriteLine();
                }
                //cara beda
                //for (int i = 0; i <= input; i++)
                //{
                //    Console.Write("2");
                //    for (int j = 0; j <= i; j += kelipatan)
                //    {
                //        if (j % 2 == 0)
                //        {
                //            Console.Write(j);
                //        }
                //        Console.Write(" ");
                //    }
                //    Console.WriteLine(" ");
                //}
            }
        }
    }
}
