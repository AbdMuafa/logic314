﻿using System;

namespace tugasLogic04
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //Console.WriteLine("Hello World!");
            menu();
        }
        static void menu()
        {
            bool ulangi = true;
            int input;
            while (ulangi)
            {
                Console.Clear();
                Console.WriteLine("\n----Menu tugas Logic 03----");
                Console.WriteLine("|   1. no 1          |");
                Console.WriteLine("|   2. no 2          |");
                Console.WriteLine("|   3. no 3          |");
                Console.WriteLine("|   4. no 4          |");
                Console.WriteLine("|   5. no 5          |");
                Console.WriteLine("|   6. no 6          |");
                Console.WriteLine("|   7. no 7          |");
                Console.WriteLine("|   8. Fibonacci     |");
                Console.WriteLine("|   9. no 9          |");
                Console.WriteLine("|   10. no 10        |");
                Console.WriteLine("|   11. no 11        |");
                Console.WriteLine("|   12. no 12        |");
                Console.WriteLine("|   0. Keluar        |");

                Console.Write("Pilih menu : ");
                input = int.Parse(Console.ReadLine());

                switch (input)
                {
                    case 1:
                        tugasNo1();
                        break;
                    case 2:
                        tugasNo2();
                        break;
                    case 3:
                        tugasNo3();
                        break;
                    case 4:
                        tugasNo4();
                        break;
                    case 5:
                        tugasNo5();
                        break;
                    case 6:
                        tugasNo6();
                        break;
                    case 7:
                        tugasNo7();
                        break;
                    case 8:
                        tugasNo8();
                        break;
                    case 9:
                        tugasNo9();
                        break;
                    case 10:
                        tugasNo10();
                        break;
                    case 11:
                        tugasNo11();
                        break;
                    case 12:
                        tugasNo12();
                        break;
                    case 13:
                        ulangi = false;
                        Console.WriteLine("Berhasil keluar");
                        break;
                    default:
                        Console.WriteLine("Masukkan angka 1 - 9!");
                        break;
                }
                ulangi = false;
            }
        }
        static void kembali()
        {
            Console.WriteLine("\nApakah ingin kembali ke menu ? (y/n) ");
            string menuInput = Console.ReadLine().ToLower();
            if (menuInput == "y")
            {
                menu();
                Console.Clear();
            }
        }

        static void tugasNo1()
        {
            float lembur, total, upah;
            float upahLembur;

            Console.WriteLine("\n--Tugas No 1 Gol Karyawan--");
            Console.WriteLine("Pilih golongan karyawan");
            Console.WriteLine("1. Golongan 1");
            Console.WriteLine("2. Golongan 2");
            Console.WriteLine("3. Golongan 3");
            Console.WriteLine("4. Golongan 4");

            Console.Write("Masukkan golongan karyawan : ");
            int golongan = int.Parse(Console.ReadLine());
            Console.Write("Masukkan jam kerja karyawan : ");
            int jamKerja = int.Parse(Console.ReadLine());

            switch (golongan)
            {
                case 1:
                    if (jamKerja > 40)
                    {
                        upah = jamKerja <= 40 ? jamKerja * 2000 : 40 * 2000;
                        lembur = jamKerja - 40;
                        upahLembur = 2000 * 1.5f * lembur;
                        total = upah + upahLembur;
                        Console.WriteLine($"Upah : {upah}");
                        Console.WriteLine($"Lembur : {lembur}");
                        Console.WriteLine($"Bayaran Lembur : {upahLembur}");
                        Console.WriteLine($"Total : {total}");
                    }
                    else if (jamKerja <= 40 && jamKerja > 0)
                    {
                        upah = jamKerja <= 40 ? jamKerja * 2000 : 40 * 2000;
                        lembur = jamKerja - 40;
                        upahLembur = 2000 * 1.5f * lembur;
                        total = upah + upahLembur;
                        Console.WriteLine($"Upah : {upah}");
                        Console.WriteLine($"Lembur : {lembur}");
                        Console.WriteLine($"Bayaran Lembur : {upahLembur}");
                        Console.WriteLine($"Total : {total}");
                    }
                    else
                    {
                        Console.WriteLine("Jam kerja tidak boleh kurang dari sama dengan 0!");
                    }
                    break;
                case 2:
                    if (jamKerja > 40)
                    {
                        upah = jamKerja <= 40 ? jamKerja * 3000 : 40 * 3000;
                        lembur = jamKerja - 40;
                        upahLembur = 3000 * 1.5f * lembur;
                        total = upah + upahLembur;
                        Console.WriteLine($"Upah : {upah}");
                        Console.WriteLine($"Lembur : {lembur}");
                        Console.WriteLine($"Bayaran Lembur : {upahLembur}");
                        Console.WriteLine($"Total : {total}");
                    }
                    else if (jamKerja <= 40 && jamKerja > 0)
                    {
                        upah = jamKerja <= 40 ? jamKerja * 3000 : 40 * 3000;
                        lembur = jamKerja - 40;
                        upahLembur = 3000 * 1.5f * lembur;
                        total = upah + upahLembur;
                        Console.WriteLine($"Upah : {upah}");
                        Console.WriteLine($"Lembur : {lembur}");
                        Console.WriteLine($"Bayaran Lembur : {upahLembur}");
                        Console.WriteLine($"Total : {total}");
                    }
                    else
                    {
                        Console.WriteLine("Jam kerja tidak boleh kurang dari sama dengan 0!");
                    }
                    break;
                case 3:
                    if (jamKerja > 40)
                    {
                        upah = jamKerja <= 40 ? jamKerja * 3000 : 40 * 4000;
                        lembur = jamKerja - 40;
                        upahLembur = 4000 * 1.5f * lembur;
                        total = upah + upahLembur;
                        Console.WriteLine($"Upah : {upah}");
                        Console.WriteLine($"Lembur : {lembur}");
                        Console.WriteLine($"Bayaran Lembur : {upahLembur}");
                        Console.WriteLine($"Total : {total}");
                    }
                    else if (jamKerja <= 40 && jamKerja > 0)
                    {
                        upah = jamKerja <= 40 ? jamKerja * 4000 : 40 * 4000;
                        lembur = jamKerja - 40;
                        upahLembur = 4000 * 1.5f * lembur;
                        total = upah + upahLembur;
                        Console.WriteLine($"Upah : {upah}");
                        Console.WriteLine($"Lembur : {lembur}");
                        Console.WriteLine($"Bayaran Lembur : {upahLembur}");
                        Console.WriteLine($"Total : {total}");
                    }
                    else
                    {
                        Console.WriteLine("Jam kerja tidak boleh kurang dari sama dengan 0!");
                    }
                    break;
                case 4:
                    if (jamKerja > 40)
                    {
                        upah = jamKerja <= 40 ? jamKerja * 5000 : 40 * 5000;
                        lembur = jamKerja - 40;
                        upahLembur = 5000 * 1.5f * lembur;
                        total = upah + upahLembur;
                        Console.WriteLine($"Upah : {upah}");
                        Console.WriteLine($"Lembur : {lembur}");
                        Console.WriteLine($"Bayaran Lembur : {upahLembur}");
                        Console.WriteLine($"Total : {total}");
                    }
                    else if (jamKerja <= 40 && jamKerja > 0)
                    {
                        upah = jamKerja <= 40 ? jamKerja * 5000 : 40 * 5000;
                        lembur = jamKerja - 40;
                        upahLembur = 5000 * 1.5f * lembur;
                        total = upah + upahLembur;
                        Console.WriteLine($"Upah : {upah}");
                        Console.WriteLine($"Lembur : {lembur}");
                        Console.WriteLine($"Bayaran Lembur : {upahLembur}");
                        Console.WriteLine($"Total : {total}");
                    }
                    else
                    {
                        Console.WriteLine("Jam kerja tidak boleh kurang dari sama dengan 0!");
                    }
                    break;
                default:
                    Console.WriteLine("Masukkan dengan benar!");
                    break;
            }
            kembali();
        }

        static void tugasNo2()
        {
            Console.WriteLine("\n--Tugas No 2 Total Kata--");
            Console.Write("Kata : ");
            string kata = Console.ReadLine();
            string[] kataSplit = kata.Split(" ");
            
            for (int i = 0; i < kataSplit.Length; i++)
            {
                Console.WriteLine($"Kata {i + 1} : {kataSplit[i]}");
            }
                Console.WriteLine("Total kata adalah " + kataSplit.Length);


            kembali();
        }

        static void tugasNo3()
        {
            Console.WriteLine("\n--Tugas No 3 Split Kata Tengah Menjadi Bintang--");
            Console.Write("Masukkan kalimat : ");
            string kalimat = Console.ReadLine();
            string[] kalimatBintang = kalimat.Split(" ");

            foreach(String s in kalimatBintang)
            {
                char[] karakter = s.ToCharArray();
                for(int a = 0; a < karakter.Length; a++)
                {
                    if(a == 0 || a == karakter.Length-1)
                    {
                        if(a == karakter.Length - 1)
                        {
                            Console.Write(karakter[a]+" ");
                        }
                        else
                        {
                            Console.Write(karakter[a]);
                        }
                    }
                    else
                    {
                        Console.Write("*");
                    }
                }
            }
            kembali();
        }

        static void tugasNo4()
        {
            Console.WriteLine("\n--Tugas No 4 Split Karakter Awal dan Akhir Menjadi Bintang--");
            Console.Write("Masukkan kalimat : ");
            string kalimat = Console.ReadLine();
            string[] kalimatBintang = kalimat.Split(" ");

            foreach (String s in kalimatBintang)
            {
                char[] karakter = s.ToCharArray();
                for (int a = 0; a < karakter.Length; a++)
                {
                    if (a == 0 || a == karakter.Length - 1)
                    {
                        if (a == karakter.Length - 1)
                        {
                        Console.Write("* ");
                        }
                        else
                        {
                            Console.Write("*");
                        }
                    }
                    else
                    {
                        Console.Write(karakter[a]);
                    }
                }
            }
            kembali();
        }

        static void tugasNo5()
        {
            Console.WriteLine("\n--Tugas No 5 Menghapus Karakter Awal--");
            Console.Write("Masukkan kalimat : ");
            string kalimat = Console.ReadLine();
            string[] kalimatBintang = kalimat.Split(" ");

            foreach (String s in kalimatBintang)
            {
                char[] karakter = s.ToCharArray();
                for (int a = 0; a < karakter.Length; a++)
                {
                    if (a == 0)
                    {
                        Console.Write(" ");
                    }
                    else
                    {
                        Console.Write(karakter[a]);
                    }
                }
            }
            kembali();
        }

        static void tugasNo6()
        {
            Console.Write("Masukkan angka = ");
            int angka = int.Parse(Console.ReadLine());
            Console.Write("Masukkan nilai pembanding : ");
            int pembanding = int.Parse(Console.ReadLine());

            Console.Write(angka + " ");
            for (int i = 0; i < angka-1; i++)
            {
                angka *= pembanding;
                if(i % 2 == 0)
                {
                    Console.Write("* ");
                }
                else
                {
                    Console.Write(angka + " ");
                }
            }
            kembali();
        }
        static void tugasNo7()
        {
            Console.Write("Masukkan angka = ");
            int angka = int.Parse(Console.ReadLine());
            Console.Write("Masukkan nilai pembanding : ");
            int pembanding = int.Parse(Console.ReadLine());
            Console.Write("Sampai suku ke - ? ");
            int suku = int.Parse(Console.ReadLine());

            for (int i = 0; i < suku; i++)
            {
                Console.Write(angka + " ");
                if (angka < 0)
                {
                    angka += (pembanding * -1);
                    angka = Math.Abs(angka);
                }
                else
                {
                    angka += pembanding;
                    angka *= -1;
                }
            }
            kembali();
        }

        //static void tugasNo6()
        //{
        //    int total = 3;

        //    Console.WriteLine("\n--Tugas No 6 Perkalian--");
        //    Console.Write("Masukkan kelipatan angka: ");
        //    int kelipatanAngka = int.Parse(Console.ReadLine());

        //    if(kelipatanAngka > 1)
        //    {
        //        for (int i = 1; i <= kelipatanAngka; i++)
        //        {
        //            total = total * kelipatanAngka;
        //            Console.Write(total + "*");
        //        }
        //    }
        //}

        //static void tugasNo6()
        //{
        //    int total = 1;
        //    int angka1=1;
        //    int angka2 = 0;
        //    Console.WriteLine("\n--Tugas No 6 Perkalian--");
        //    Console.Write("Masukkan kelipatan angka: ");
        //    int kelipatanAngka = int.Parse(Console.ReadLine());
        //    for (int i = 0; i < kelipatanAngka; i++)
        //    {
        //        total = angka1 + kelipatanAngka;
        //        angka2 = angka1;
        //        angka1 = total;
        //        Console.Write(total + "*");
        //    }
        //}
        static void tugasNo9()
        {
            kembali();
        }
        static void tugasNo10()
        {
            int harga;
            string merk;
            Console.WriteLine("\n--Tugas No 10 Pakaian--");
            Console.WriteLine("Merk Baju : ");
            Console.WriteLine("1. IMP");
            Console.WriteLine("2. Prada");
            Console.WriteLine("3. Gucci");
            Console.Write("Masukkan kode baju = ");
            int kodeBaju = int.Parse(Console.ReadLine());
            Console.Write("Masukkan kode ukuran baju = ");
            string ukuranBaju = Console.ReadLine().ToLower();

            if(ukuranBaju.GetType() != typeof(string))
            {
                Console.WriteLine("input ukuran baju invalid!");
            }

            switch (kodeBaju)
            {
                case 1:
                    merk = "IMP";
                    if (ukuranBaju == "s")
                    {
                        harga = 200000;
                        Console.WriteLine("Merk Baju = " + merk);
                        Console.WriteLine("Harga Baju = " + harga);
                    }
                    else if(ukuranBaju == "m")
                    {
                        harga = 220000;
                        Console.WriteLine("Merk Baju = " + merk);
                        Console.WriteLine("Harga Baju = " + harga);
                    }
                    else if(ukuranBaju == "l" && ukuranBaju == "xl" && ukuranBaju == "xxl")
                    {
                        harga = 250000;
                        Console.WriteLine("Merk Baju = " + merk);
                        Console.WriteLine("Harga Baju = " + harga);
                    }
                    else
                    {
                        Console.WriteLine("Maaf ukuran belum tersedia");
                    }
                    break;
                case 2:
                    merk = "Prada";
                    if (ukuranBaju == "s")
                    {
                        harga = 150000;
                        Console.WriteLine("Merk Baju = " + merk);
                        Console.WriteLine("Harga Baju = " + harga);
                    }
                    else if (ukuranBaju == "m")
                    {
                        harga = 160000;
                        Console.WriteLine("Merk Baju = " + merk);
                        Console.WriteLine("Harga Baju = " + harga);
                    }
                    else if (ukuranBaju == "l" && ukuranBaju == "xl" && ukuranBaju == "xxl")
                    {
                        harga = 170000;
                        Console.WriteLine("Merk Baju = " + merk);
                        Console.WriteLine("Harga Baju = " + harga);
                    }
                    break;
                case 3:
                    merk = "Gucci";
                    if(ukuranBaju == "s" && ukuranBaju == "m" && ukuranBaju=="l" && ukuranBaju== "xl" && ukuranBaju == "xxl")
                    {
                        harga = 200000;
                        Console.WriteLine("Merk Baju = " + merk);
                        Console.WriteLine("Harga Baju = " + harga);
                    }
                    break;
                default:
                    Console.WriteLine("Kode baju yang anda masukkan tidak tersedia!");
                    break;
            }
            kembali();
        }
        static void tugasNo11()
        {
            Console.WriteLine("\n--Baju Lebaran--");
            Console.Write("Masukkan nama : ");
            string nama = Console.ReadLine();
            Console.Write($"Uang {nama} : Rp.");
            int uang = int.Parse(Console.ReadLine());
            Console.Write("Harga baju (pakai koma \',\') : ");
            int[] hargaBaju = Array.ConvertAll(Console.ReadLine().Split(","), int.Parse);
            Console.Write("Harga celana (pakai koma \',\') : ");
            int[] hargaCelana = Array.ConvertAll(Console.ReadLine().Split(","), int.Parse);

            int maxBelanja = 0;
            for(int i = 0; i < hargaBaju.Length; i++)
            {
                for(int j = 0; j < hargaCelana.Length; j++)
                {
                    int harga = hargaBaju[i] + hargaCelana[j];
                    if(harga <= uang && harga >= maxBelanja)
                    {
                        maxBelanja = harga;
                        Console.WriteLine(maxBelanja);
                    }
                }
            }

            kembali();
        }

        static void tugasNo12()
        {
            Console.WriteLine("\n--Tugas No 12 Pola Persegi--");
            Console.Write("Masukkan jumlah angka : ");
            int persegi = int.Parse(Console.ReadLine());

            // for pertama untuk baris, for kedua baru kolom

            for(int i = 1; i <= persegi; i++)
            {
                for (int j = 1; j <= persegi; j++)
                {
                    if(  i == 1)
                    {
                        Console.Write(j+"\t");
                    }
                    else if(i == persegi)
                    {
                        Console.Write(j);
                    }
                    else if( j == 1 || j == persegi)
                    {
                        Console.Write("*\t");
                    }
                    else
                    {
                        Console.Write("\t");
                    }
                }
                Console.WriteLine("");
            }
        }

        static void tugasNo8()
        {
            int angka1 = 0;
            int angka2 = 1;
            int angka3 = 1;

            Console.WriteLine("\n--Tugas No 8 Fibonacci--");
            Console.Write("Masukkan jumlah bilangan fibonacci : ");
            int fibon = int.Parse(Console.ReadLine());
            for (int i = 0; i < fibon; i++)
            {
                angka3 = angka2 + angka1;
                angka1 = angka2;
                angka2 = angka3;
                Console.Write(angka1+",");
                //if (angka1 <= fibon)
                //{
                //    Console.Write(angka1+",");
                //}
                //else
                //{
                //    Console.Write(",");
                //}
            }
                Console.WriteLine();
        }

        static void forEachSum()
        {
            int[] namaArray = { 34, 35, 67 };
            int sum = 0;
            foreach(int n in namaArray)
            {
                sum += n;
            }
            Console.WriteLine(sum);
        }
    }
}
