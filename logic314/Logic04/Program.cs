﻿using System;

namespace Logic04
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //removeString();
            insertString();
            Console.ReadKey();
        }
        static void removeString()
        {
            Console.WriteLine("--Remove String--");
            Console.Write("Masukkan Kalimat : ");
            string kalimat = Console.ReadLine();
            Console.Write("Remove kalimat yang ke- : ");
            int param = int.Parse(Console.ReadLine());

            Console.WriteLine($"Hasil remove string : {kalimat.Remove(param)}");
        }
        static void insertString()
        {
            Console.WriteLine("--Insert String--");
            Console.Write("Masukkan Kalimat : ");
            string kalimat = Console.ReadLine();
            Console.Write("Isi parameter Insert : ");
            int param = int.Parse(Console.ReadLine());
            Console.Write("Masukkan input terbaru : ");
            string input = Console.ReadLine();

            Console.WriteLine($"Hasil insert string : {kalimat.Insert(param, input)}");
        }
        static void replaceString()
        {

        }
    }
}
