﻿using System;
using System.Collections.Generic;

namespace Logic10
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //simpleArraySum();
            staircase();
            Console.ReadKey();
        }
        static void simpleArraySum()
        {
            Console.WriteLine("---Simple Array Sum---");
            Console.Write("");

            //cara 1 hard code
            List<int> list2 = new List<int>();
            list2.Add(1);
            list2.Add(2);
            list2.Add(3);
            list2.Add(4);
            list2.Add(10);
            list2.Add(11);

            //CONTOH HARD CODE, jika ingin looping gunakan list.Add
            List<int> list = new List<int>() { 1, 2, 3, 4, 10, 11 };
            int hasil = simpleArraysum(list2);
            Console.WriteLine(hasil);
        }
        static int simpleArraysum(List<int> ar)
        {
            int hasil = 0;
            for (int i = 0; i < ar.Count; i++)
            {
                hasil += ar[i];
            }
            return hasil;
        }

        static void staircase() {
            Console.WriteLine("---Stair Case---");
            Console.Write("Masukkan angka : ");
            int n = int.Parse(Console.ReadLine());
            stairCase(n);
        }
        static void stairCase(int n)
        {
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    if (j < n - 1 - i)
                    {
                        Console.Write(" ");
                    }
                    else
                    {
                        Console.Write("#");
                    }
                }
                Console.WriteLine();
            }
            
        }
    }
}
