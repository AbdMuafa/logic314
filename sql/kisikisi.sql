-- kisi kisi
create database db_kisikisi
use db_kisikisi
create table assignment(
	id int primary key identity(1,1),
	name varchar(50) not null,
	marks int not null
)

alter table assignment alter column marks varchar(50) null

insert into assignment(name, marks) values
('Isni', 85),
('Laudrey', 75),
('Bambang', 40),
('Anwar', 91),
('Alwi',70),
('Fulan', 50)

delete from assignment

select name, marks from assignment

select name, marks,
case
when marks > 90 then  'A+'
when marks > 70 then 'A'
when marks > 60 then 'B'
when marks > 40 then 'C'
else 'Fail'
end
as grade from assignment

