--Tugas Day 01
Create database dbPenerbit
use dbPenerbit
create table tbPengarang(
	id bigint primary key identity (1,1),
	kd_pengarang varchar(7) not null,
	nama varchar(30) not null,
	alamat varchar(80) not null,
	kota varchar(15) not null,
	kelamin varchar(1) not null
)

insert into tbPengarang(kd_pengarang, nama, alamat, kota, kelamin)
values('P0001','Ashadi','Jl. Beo 25','Yogya','P'),
('P0002','Rian','Jl. Solo 123','Yogya','P'),
('P003','Suwadi','Jl. Semangka 13','Bandung','P'),
('P0004','Siti','Jl. Durian 15','Solo','W'),
('P0005','Amir','Jl. Gajah 33','Kudus','P'),
('P0006','Suparman','Jl. Harimau 25','Jakarta','P'),
('P0007','Jaja','Jl. Singa 7','Bandung','P'),
('P0008','Saman','Jl. Naga 12','Yogya','P'),
('P0009','Anwar','Jl. Tidar 6A','Magelang','P'),
('P00010','Fatmawati','Jl. Renjana 4','Bogor','W')

update tbPengarang set kd_pengarang = 'P0010' where nama = 'Fatmawati';

create table tblGaji(
	ID bigint primary key identity (1,1),
	Kd_Pengarang varchar(7) not null,
	Nama varchar(30) not null,
	Gaji decimal(18,4) not null,
)

insert into tblGaji values('P0002', 'Rian', 600000),
('P0005', 'Amir', 700000),
('P0004', 'Siti', 500000),
('P0003', 'Suwadi', 1000000),
('P0010', 'Fatmawati', 600000),
('P0008', 'Saman', 750000)

update tblGaji set kd_pengarang = 'P0010' where id = 5

--hitung dan tampilkan jumlah pengarang
select count(id),nama from tbPengarang group by nama
select count(id) as Jumlah_Pengarang from tbPengarang

--No 2
select kelamin,
case when kelamin = 'P' then count(kelamin)
when kelamin = 'W' then count(kelamin)
else 'Tidak ada'
end
from tbPengarang group by kelamin

--no 2 cara pendek
select kelamin, count(id) as jumlah_kelamin
from tbPengarang group by kelamin

select count(kelamin) as Total_Pria from tbPengarang where kelamin = 'P'
select count(kelamin) as Total_Wanita from tbPengarang where kelamin = 'W'

-- no 3 record kota dan jumlah kota dari tbPengarang
select Kota, count(kota) as Record_Kota from tbPengarang group by kota
select count(kota) from tbPengarang

--no 4 record kota diatas 1
select kota, count(kota) as Record_Kota from tbPengarang group by kota having count(kota) > 1

--no 5 kd_pengarang terbesar dan terkecil
select 
max(substring(kd_pengarang, 1, 5)) as kd_pengarang_terbesar, 
min(substring(kd_pengarang, 1, 5)) as kd_pengarang_terendah 
from tbPengarang
--cara dua no 5
select 
max(kd_pengarang) as Kode_Pengarang_Terbesar, 
MIN(kd_pengarang) as Kode_Pengarang_Terendah 
from tbPengarang
--mengurutkan dan tampil semuanya
select kd_pengarang from tbPengarang order by kd_pengarang desc

--no6 gaji tertinggi dan terendah
select max(gaji) as gaji_tertinggi, min(gaji) as gaji_terendah from tblGaji

--no7  gaji diatas 600000
select nama, gaji from tblGaji where gaji > 600000

--no8 jumlah gaji
select sum(gaji) as Jumlah_Gaji from tblGaji

--no 9 jumlah gaji berdasarkan kota
select kota,sum(gaji) as jumlah_gaji 
from tblGaji as gaji
join tbPengarang as pgrg 
on gaji.Kd_Pengarang = pgrg.kd_pengarang 
group by kota

-- no10 tampilkan seluruh record antara P0003 - P0006
select * from tbPengarang where kd_pengarang between 'P0003' and 'P0006'
select * from tbPengarang where kd_pengarang >= 'P0003' and kd_pengarang <= 'P0006'

--NO 11 SELURUH DATA YOGYA, SOLO, DAN MAGELANG
select * from tbPengarang where kota = 'yogya' or kota = 'solo' or kota = 'magelang'
--cara 2 no 11
select * from tbPengarang where kota in('yogya', 'solo', 'magelang')

-- NO12 SELURUH DATA KECUALI YOGYA
select * from tbPengarang where kota <> 'yogya'
select * from tbPengarang where not kota = 'yogya'

--NO13 SELURUH DATA PENGARANG A. DIMULAI HURUF A, B. BERKAHIRAN I, C. HURUF KETIGA a, D. tidak berakhiran n
select * from tbPengarang where nama LIKE 'a%'
select * from tbPengarang where nama LIKE '%i' and kd_pengarang Like '%4'
select * from tbPengarang where nama LIKE '__a%'
select * from tbPengarang where nama not like '%n'

--no 14 tampilkan data tblPengarang dan tblGaji dengan kd_pengarang yg sama
select *
from tblGaji as gaji
join tbPengarang as pgrg 
on gaji.Kd_Pengarang = pgrg.kd_pengarang 

select *
from tbPengarang as pgrg
join tblGaji as gaji 
on gaji.Kd_Pengarang = pgrg.kd_pengarang 

--no15 tampilkan kota yang gaji dibawah 1 juta
select kota, gaji 
from tblGaji as gaji 
join tbPengarang as pgrg 
on gaji.Kd_Pengarang = pgrg.kd_pengarang 
where gaji < 1000000

--no16 ubah panjang dari tipe kelamin menjadi 10
alter table tbPengarang alter column kelamin varchar(10)

--no 17 tambahkan kolom gelar, varchar (12) di tblPengarang
alter table tbPengarang add gelar varchar(12) not null

-- no 18 ubah alamat dan kota dari rian menjadi Jl. Cendrawasih 65 dan Pekanbaru
update tbPengarang set 
alamat = 'Jl. Cendrawasih 65', 
kota = 'Pekanbaru' 
where nama = 'rian';

select * from tbPengarang
--no 19 view untuk attribut kd_pengarang, nama, kota, gaji, dengan nama vwPengarang
create view vwPengarang
as 
select *
from tblGaji as gaji
join tbPengarang as pgrg 
on gaji.Kd_Pengarang = pgrg.kd_pengarang

--menampilkan full data
select *
from tblGaji as gaji
full outer join tbPengarang as pgrg 
on gaji.Kd_Pengarang = pgrg.kd_pengarang 

select * from vwPengarang

--menampilkan struktur table
sp_columns 'tbPengarang'
sp_help 'tbPengarang'

--row number
select *, ROW_NUMBER() over(order by nama) as nomor_urut from tbPengarang