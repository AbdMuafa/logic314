--TUGAS SQL DAY 02
create database dbEntertainer
use dbEntertainer

--create tabel
create table artis(
	kd_Artis varchar(10) primary key,
	nm_artis varchar(50),
	jk varchar(10),
	bayaran int,
	award int,
	negara varchar(5),
)

alter table artis alter column bayaran bigint

UPDATE artis set bayaran = 3000000000 where kd_Artis = 'A001'

INSERT INTO artis VALUES('A001', 'ROBERT DOWNEY JR', 'PRIA', 10000000000, 2, 'AS')
INSERT INTO artis VALUES('A002', 'ANGELINA JOLIE', 'WANITA', 700000000, 1, 'AS'),
('A003', 'JACKIE CHAN', 'PRIA', 200000000, 7, 'HK'),
('A004', 'JOE TASLIM', 'PRIA', 350000000, 1, 'ID'),
('A005', 'CHELSEA ISLAN', 'WANITA', 300000000, 0, 'ID')

use dbEntertainer
SELECT * FROM artis

-- create table film
create table film(
	kd_film varchar(10) primary key,
	nm_film varchar(50),
	genre varchar(50),
	artis varchar(50),
	produser varchar(50),
	pendapatan bigint,
	nominasi smallint
)

select * from film

drop table film

alter table film drop column nominal
alter table film add artis smallint

INSERT INTO film
VALUES('F001', 'IRON MAN', 'G001', 'A001', 'PD01', 2000000000, 3),
('F002', 'IRON MAN 2', 'G001', 'A001', 'PD01', 1800000000, 2),
('F003', 'IRON MAN 3', 'G001', 'A001', 'PD01', 1200000000, 0),
('F004', 'AVENGER-CIVIL WAR', 'G001', 'A001', 'PD01', 2000000000, 1),
('F005', 'SPIDERMAN HOME COMING', 'G001', 'A001', 'PD01', 1300000000, 0),
('F006', 'THE RAID', 'G001', 'A004', 'PD03', 800000000, 5),
('F007', 'FAST & FURIOUS', 'G001', 'A004', 'PD05', 830000000, 2),
('F008', 'HABIBIE DAN AINUN', 'G004', 'A005', 'PD03', 670000000, 4),
('F009', 'POLICE STORY', 'G001', 'A003', 'PD02', 700000000, 3),
('F010', 'POLICE STORY 2', 'G001', 'A003', 'PD02', 710000000, 1),
('F011', 'POLICE STORY 3', 'G001', 'A003', 'PD02', 615000000, 0),
('F012', 'RUSH HOUR', 'G003', 'A003', 'PD05', 695000000, 2),
('F013', 'KUNGFU PANDA', 'G003', 'A003', 'PD05', 923000000, 5)

--create tabel
create table produser(
	kd_produser varchar(10) primary key,
	nm_produser varchar(50),
	international varchar(50),
)

insert into produser values
('PD01', 'MARVEL', 'YA'),
('PD02', 'HONGKONG CINEMA', 'YA'),
('PD03', 'RAPI FILM', 'TIDAK'),
('PD04', 'PARKIT', 'TIDAK'),
('PD05', 'PARAMOUNT PICTURE', 'YA')

SELECT * FROM produser

create table negara(
	kd_negara varchar(10) primary key,
	nm_negara varchar(100)
)

insert into negara values
('AS', 'AMERIKA SERIKAT'),
('HK', 'HONGKONG'),
('ID', 'INDONESIA'),
('IN', 'INDIA')

SELECT * FROM negara

create table genre(
	kd_genre varchar(20) primary key,
	nm_genre varchar(50)
)
insert into genre values
('G001', 'ACTION'),
('G002', 'HORROR'),
('G003', 'COMEDY'),
('G004', 'DRAMA'),
('G005', 'THRILLER')

SELECT * FROM genre

select kota,sum(gaji) as jumlah_gaji 
from tblGaji as gaji
join tbPengarang as pgrg 
on gaji.Kd_Pengarang = pgrg.kd_pengarang 
group by kota

--NO1 MENAMPILKAN JUMLAH PENDAPATAN PRODUSER MARVEL
select produser.nm_produser, sum(film.pendapatan) as pendapatan
from film 
join produser
on film.produser = produser.kd_produser 
where produser.nm_produser = 'MARVEL'
group by produser.nm_produser

select * from produser
select * from film

-- no 2 menampilkan nama film dan yang tidak mendapatkan nominasi
select nm_film, nominasi from film where nominasi = 0

-- no3 menampilkan nama film yang huruf depannya P
select nm_film from film where nm_film like 'P%'

--no4  menampilkan nama film yang huruf terakhir y
select nm_film from film where nm_film like '%y'

--no 5 menampilkan nama film yang mendaung huruf d
select nm_film from film where nm_film like '%d%'

--no 6 menampilkan nama film dan artis
select artis.nm_artis, film.nm_film
from artis
join film
on artis.kd_Artis = film.artis

-- cara 2
select artis.nm_artis, film.nm_film
from film, artis where artis = kd_Artis

--no7 menampilkan nama film yang artisnya berasal dari negara hongkong
select film.nm_film, artis.negara
from film
join artis
on film.artis = artis.kd_Artis
where negara = 'hk'

--no8 menampilkan nama film yang artisnya bkn dari negara yg mengandung huruf o
select film.nm_film, negara.nm_negara
from film
join artis on film.artis = artis.kd_Artis 
join negara on negara.kd_negara = artis.negara
where negara.nm_negara <> '%o%'

--no 9 menampilkan nama artis yang tidak pernah bermain film
select nm_artis
from artis
left join film on film.artis = artis.kd_Artis
where film.nm_film is null

--no 10 menampilkan nama artis yang bermain film
select artis.nm_artis, genre.nm_genre
from film
join genre on film.genre = genre.kd_genre
join artis on artis.kd_Artis = film.artis
where genre.nm_genre = 'drama'

use DB_Entertainer

--no11 menampilkan nama artis bermain genre action
--bisa pakai distinct agar tidak ada data yg terduplicate
select distinct artis.nm_artis, genre.nm_genre
from film
join genre on film.genre = genre.kd_genre
join artis on artis.kd_Artis = film.artis
where genre.nm_genre = 'action'

--cara 2 no 11
select artis.nm_artis, genre.nm_genre
from film
join genre on film.genre = genre.kd_genre
join artis on artis.kd_Artis = film.artis
where genre.nm_genre = 'action'
group by genre.nm_genre, artis.nm_artis
order by artis.nm_artis desc

--no12 menampilkan data negara dengan jumlah filmnya
select kd_negara, nm_negara, count(film.nm_film) as jumlah_film
from artis 
join film on film.artis = artis.kd_artis 
right join negara on negara.kd_negara = artis.negara
group by kd_negara, nm_negara

-- no 13 menampilkan nama film yang skala internasional
select nm_film from film
join produser on film.produser = produser.kd_produser
where produser.international = 'ya'

-- no 14 menampilkan jumlah film dari masing masing produser
select nm_produser, count(nm_film) as jumlah_Film
from produser
left join film on film.produser = produser.kd_produser
group by nm_produser


