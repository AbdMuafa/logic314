-- tugas sql days 03

create database dbSales
use dbSales

create table SALESPERSON(
	ID int primary key identity(1,1),
	NAME varchar(50) not null,
	BOD date not null,
	SALARY decimal(18,2) not null
)

insert into SALESPERSON values
('Abe', '09-11-1988', 140000),
('Bob', '09-11-1978', 44000),
('Chris', '09-11-1983', 40000),
('Dan', '09-11-1980', 52000),
('Ken', '09-11-1977', 115000),
('Joe', '09-11-1990', 38000)

update SALESPERSON set BOD = '1978/11/9' where id = 2

select * from salesperson

create table ORDERS(
	ID INT PRIMARY KEY IDENTITY(1,1),
	ORDER_DATE DATE NOT NULL,
	CUST_ID INT NULL,
	SALESPERSON_ID INT NOT NULL,
	AMOUNT DECIMAL(18,2) NOT NULL
)

INSERT INTO ORDERS VALUES
('2020-08-02', 4 , 2 , 540),
('2021-01-22', 4 , 5 , 1800),
('2019-07-14', 9, 1, 46),
('2018-01-29', 7 , 2 , 2400),
('2021-02-03', 6 , 4 , 600),
('2020-03-02', 6 , 4 , 600),
('2021-05-06', 9 , 4 , 150)

SELECT * FROM ORDERS

--JAWABAN
--A.
select sp.name, count(ord.id)[jumlah_order] 
from SALESPERSON as sp
join ORDERS ord on sp.id = ord.SALESPERSON_ID
group by sp.NAME
having COUNT(ord.ID) > 1

--B.
select sal.NAME, sum(ord.AMOUNT)[total_Amount]
from SALESPERSON as sal
join ORDERS ord 
on sal.id = ord.SALESPERSON_ID
group by sal.NAME
having sum(ord.AMOUNT) > 1000

select sp.name, count(ord.id)[jumlah_order] 
from SALESPERSON as sp
join ORDERS ord on sp.id = ord.SALESPERSON_ID
group by sp.NAME
having sum(ord.ID) > 1000

--C.
select sal.name, DATEDIFF(year, sal.BOD, getdate())[UMUR], sal.salary, 
sum(ord.amount)[total_amount]
from SALESPERSON sal
join orders ord on sal.ID = ord.SALESPERSON_ID
where YEAR(ord.order_date) >= 2020
group by sal.NAME, sal.SALARY, sal.BOD
order by umur asc

--D.
select sal.name, AVG(ord.amount)[ratarata_totalAmount]
from SALESPERSON sal
join ORDERS ord on sal.ID = ord.SALESPERSON_ID
group by sal.NAME
order by ratarata_totalAmount desc

-- jika hanya ingin memunculkan satu gunakan top contoh top 1
use dbSales
select top 1 sal.name, AVG(ord.amount)[ratarata_totalAmount]
from SALESPERSON sal
join ORDERS ord on sal.ID = ord.SALESPERSON_ID
group by sal.NAME
order by ratarata_totalAmount desc

--E 
select sal.NAME, count(ord.id)[Total_Order], 
sum(ord.AMOUNT)[Total_Amount], SAL.SALARY * 0.3 [BONUS], sal.SALARY * 0.3 + sal.SALARY [TotalBonus_Gaji]
from SALESPERSON sal
join ORDERS ord on sal.ID = ord.SALESPERSON_ID
group by sal.NAME, sal.SALARY
having count(ord.id) > 2 and sum(ord.AMOUNT) > 1000

--F
select NAME from SALESPERSON sal
left join ORDERS ord
on sal.ID = ord.SALESPERSON_ID
where ord.SALESPERSON_ID is null

--G
select NAME, sal.SALARY * 0.98 [Potongan_danGaji], sal.SALARY * 0.02 [Potongan]
from SALESPERSON sal
left join ORDERS ord
on sal.ID = ord.SALESPERSON_ID
where ord.SALESPERSON_ID is null
