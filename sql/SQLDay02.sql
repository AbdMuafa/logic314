--sqlday02

--cast
select CAST(10 as decimal(18,4))
--string to int
select CAST('10' as int)

select CAST(10.65 as int)

--string menjadi date
select cast('2023-03-16' as date)

--mencetak tanggal atau waktu hari ini
select GETDATE(), GETUTCDATE()

select day(getdate()) as Tanggal, MONTH(GETDATE()) as Bulan, YEAR(GETDATE()) as Tahun

--Convert
select CONVERT(decimal(18,2), 10123)
select convert(int, '10') as stringTOint
select convert(int, 10.65) as desimal_to_int
select convert(datetime, '2023-03-16')

--dateadd
select DATEADD(year,2,getdate()) as pertama, DATEADD(month,3,GETDATE()) as kedua, DATEADD(day,5,GETDATE())

--datediff
select DATEDIFF(day, '2023-03-16', '2023-03-25') as perbedaan_hari, DATEDIFF(month, '2023-03-16', '2024-06-16') as perbedaan_bulan

--subquery
use db_kampus
insert into [dbo].[mahasiswa]
select name, address, email,panjang from mahasiswa
where panjang = (select max(panjang) from mahasiswa)

select * from mahasiswa

delete from mahasiswa where id > 5

--create view
create view vwMahasiswa
as
select * from mahasiswa

--create index
create index index_name
on mahasiswa(name)

create index index_address_Email
on mahasiswa(address,email)

select * from mahasiswa where name = 'toni'

-- create unique index
create unique index uniqueindex_address
on mahasiswa(address)

--drop index
drop index index_address_Email on mahasiswa

drop index uniqueindex_address
on mahasiswa

select * from mahasiswa
select * from biodata

-- add primary key menjadi composite
alter table mahasiswa add constraint pk_Address primary key(id, address)

--drop primary key constraint
alter table mahasiswa drop constraint pk_Address

--add unique constrain
alter table mahasiswa add constraint unique_address unique(address)

--drop unique costrain
alter table mahasiswa drop constraint unique_address

