--TugasSQLDAY03 - VER 02

create database dbHR
use dbHR
create table tb_karyawan(
	id bigint primary key identity(1,1),
	nip varchar(50) not null,
	nama_depan varchar(50) not null,
	nama_belakang varchar(50) not null,
	jenis_kelamin varchar(50) not null,
	agama varchar(50) not null,
	tempat_lahir varchar(50) not null,
	tgl_lahir date,
	alamat varchar(100) not null,
	pendidikan_terakhir varchar(50) not null,
	tgl_masuk date
)

insert into tb_karyawan values
('001','Hamidi', 'Samsudin','Pria','Islam','Sukabumi','1977-04-21','Jl. Sudirman No. 12', 'S1 Teknik Mesin', '2015-12-07'),
('002','Ghandi', 'Wamida','Wanita','Islam','Palu','1992-01-12','Jl. Rambutan No.22', 'SMA Negeri 02 Palu', '2014-12-01'),
('003','Paul', 'Christian','Pria','Kristen','Ambon','1980-05-27','Jl. Veteran No.4', 'S1 Pendidikan Geografi', '2014-01-12')

select * from tb_karyawan
select * from tb_divisi

create table tb_divisi
(
	id bigint primary key identity(1,1),
	kd_divisi varchar (50) not null,
	nama_divisi varchar (50) not null
)

insert into tb_divisi values
('GD','Gudang'),
('HRD','HRD'),
('KU','Keuangan'),
('UM','Umum')

create table tb_jabatan
(
	id bigint primary key identity(1,1),
	kd_jabatan varchar (50) not null,
	nama_jabatan varchar(50) not null,
	gaji_pokok bigint,
	tunjangan_jabatan bigint
)

insert into tb_jabatan values
( 'MGR','Manager',5500000,1500000 ),
( 'OB','Office Boy',1900000,200000),
( 'ST','Staff',3000000,750000),
( 'WMGR','Wakil Manager',4000000,1200000)

create table tb_pekerjaan(
	id bigint primary key identity(1,1),
	nip varchar(50) not null,
	kode_jabatan varchar(50) not null,
	kode_divisi varchar(50) not null,
	tunjangan_kinerja bigint,
	kota_penempatan varchar(50)
)

insert into tb_pekerjaan values
('003','MGR','HRD',1500000, 'Sukabumi')

select * from tb_pekerjaan

--no1 tampilkan nama lengkap, nama jabatan, tunjangan jabatan +gaji, yang gaji+tunjangan kinerja dibawah 5 juta
select concat(kar.nama_depan,' ',kar.nama_belakang) as nama_lengkap, 
nama_jabatan, gaji_pokok+tunjangan_jabatan as gaji_tunjangan
from tb_karyawan as kar
join tb_pekerjaan as pkr 
on kar.nip = pkr.nip
join tb_jabatan as jab
on pkr.kode_jabatan = jab.kd_jabatan
where gaji_pokok+tunjangan_jabatan < 5000000


select * from tb_karyawan
select* from tb_jabatan
select * from tb_pekerjaan
select * from tb_divisi

--No 2 tampilkan nama lengkap, jabatan, nama divisi, total gaji, pajak, gaji bersih, yg gender pria dan penempatan kerja diluar sukabumi
select concat(kar.nama_depan, ' ', kar.nama_belakang) as nama_lengkap,
nama_jabatan, div.nama_divisi, jab.gaji_pokok+jab.tunjangan_jabatan+pkr.tunjangan_kinerja as total_gaji,
(jab.gaji_pokok+jab.tunjangan_jabatan+pkr.tunjangan_kinerja) * 0.05 as Pajak,
(jab.gaji_pokok+jab.tunjangan_jabatan+pkr.tunjangan_kinerja) - ((jab.gaji_pokok+jab.tunjangan_jabatan+pkr.tunjangan_kinerja) * 0.05) as Gaji_Bersih
from tb_karyawan as kar
join tb_pekerjaan as pkr 
on kar.nip = pkr.nip
join tb_jabatan as jab
on pkr.kode_jabatan = jab.kd_jabatan
join tb_divisi as div
on div.kd_divisi = pkr.kode_divisi
where kar.jenis_kelamin = 'pria' or pkr.kota_penempatan not like 'sukabumi'

--No3 tampilkan nama lengkap, jabatan, nama divisi, bonus(bonus = 25% dari total gaji(gaji pokok+tunjangan_jabatan+tunjangan kinerja)*7)
select kar.nip, concat(kar.nama_depan, ' ', kar.nama_belakang) as nama_lengkap,
nama_jabatan, div.nama_divisi,
((jab.gaji_pokok+jab.tunjangan_jabatan+pkr.tunjangan_kinerja) / 4 ) * 7 as Bonus
from tb_pekerjaan as pkr 
right join tb_karyawan as kar
on kar.nip = pkr.nip
left join tb_jabatan as jab
on pkr.kode_jabatan = jab.kd_jabatan
left join tb_divisi as div
on div.kd_divisi = pkr.kode_divisi
order by kar.jenis_kelamin asc

--no 4 tampilkan nama lengkap, total gaji, infak(5%*total gaji) yang mempu yai jabatan mgr
select kar.nip, concat(kar.nama_depan, ' ', kar.nama_belakang) as nama_lengkap,
nama_jabatan, div.nama_divisi, jab.gaji_pokok+jab.tunjangan_jabatan+pkr.tunjangan_kinerja as total_gaji,
((jab.gaji_pokok+jab.tunjangan_jabatan+pkr.tunjangan_kinerja) * 0.05 ) as Infak
from tb_pekerjaan as pkr 
right join tb_karyawan as kar
on kar.nip = pkr.nip
left join tb_jabatan as jab
on pkr.kode_jabatan = jab.kd_jabatan
left join tb_divisi as div
on div.kd_divisi = pkr.kode_divisi
where jab.kd_jabatan = 'MGR'

-- no 5 tampilkan nama lengkap, nama jabatan, pendidikan terakhir, tunjangan pendidikan, total gaji(gapok+tjabatan+pendidikan) pendidikan akhirnya s1
select kar.nip, concat(kar.nama_depan, ' ', kar.nama_belakang) as nama_lengkap,
nama_jabatan, kar.pendidikan_terakhir,2000000[kar.tunjangan_pendidikan], 
jab.gaji_pokok+jab.tunjangan_jabatan+2000000 as total_gaji
from tb_pekerjaan as pkr 
right join tb_karyawan as kar
on kar.nip = pkr.nip
left join tb_jabatan as jab
on pkr.kode_jabatan = jab.kd_jabatan
left join tb_divisi as div
on div.kd_divisi = pkr.kode_divisi
where kar.pendidikan_terakhir like 'S1%'

--no 6 tampilkan nip nama lengkap, jabatan, nama divisi, bonus
select kar.nip, 
concat(kar.nama_depan, ' ', kar.nama_belakang) as nama_lengkap,
nama_jabatan, div.nama_divisi,
case
	when jab.kd_jabatan = 'MGR' then ((jab.gaji_pokok+jab.tunjangan_jabatan+pkr.tunjangan_kinerja) / 4 ) * 7
	when jab.kd_jabatan = 'ST' then ((jab.gaji_pokok+jab.tunjangan_jabatan+pkr.tunjangan_kinerja) / 4 ) * 5
	ELSE ((jab.gaji_pokok+jab.tunjangan_jabatan+pkr.tunjangan_kinerja) / 4 ) * 2
END as Bonus
from tb_pekerjaan as pkr 
right join tb_karyawan as kar
on kar.nip = pkr.nip
left join tb_jabatan as jab
on pkr.kode_jabatan = jab.kd_jabatan
left join tb_divisi as div
on div.kd_divisi = pkr.kode_divisi
order by kar.jenis_kelamin asc

--no 7 membuat kolom nip table karyawan menjadi kolom unique
alter table tb_karyawan add unique(nip)

--no 8 membuat kolom nip pada table karyawan sebagai index
create index karyawan on tb_karyawan(nip)

--no9 tampilkan nama lengkap, nama belakang diubah menjadi huruf capital dengan kondisi nama belakang di awali dengan huruf w
select Concat(nama_depan,' ', upper(nama_belakang))
from tb_karyawan
where nama_belakang like 'W%'

select 
Concat(nama_depan,' ', nama_belakang) as nama_lengkap,
case when nama_belakang like 'W%' then upper(nama_belakang)
end
from tb_karyawan

-- no 10
select kar.nip, 
concat(kar.nama_depan, ' ', kar.nama_belakang) as nama_lengkap,
kar.tgl_masuk, nama_jabatan, div.nama_divisi, 
(jab.gaji_pokok+jab.tunjangan_jabatan+pkr.tunjangan_kinerja) as total_gaji,
DATEDIFF(year,kar.tgl_masuk, GETDATE()) as lama_kerja,
case
	when jab.kd_jabatan = 'MGR' then ((jab.gaji_pokok+jab.tunjangan_jabatan+pkr.tunjangan_kinerja) / 4 ) * 7
	when jab.kd_jabatan = 'ST' then ((jab.gaji_pokok+jab.tunjangan_jabatan+pkr.tunjangan_kinerja) / 4 ) * 5
	ELSE ((jab.gaji_pokok+jab.tunjangan_jabatan+pkr.tunjangan_kinerja) / 4 ) * 2
END as Bonus
from tb_pekerjaan as pkr 
right join tb_karyawan as kar
on kar.nip = pkr.nip
left join tb_jabatan as jab
on pkr.kode_jabatan = jab.kd_jabatan
left join tb_divisi as div
on div.kd_divisi = pkr.kode_divisi
where DATEDIFF(year,kar.tgl_masuk,GETDATE()) >= 8

select k.nip, k.nama_depan + ' ' + k.nama_belakang as nama_lengkap, k.tgl_masuk,
j.nama_jabatan, d.nama_divisi,(j.gaji_pokok + j.tunjangan_jabatan + p.tunjangan_kinerja)as total_gaji,
DATEDIFF(YEAR,k.tgl_masuk, GETDATE()) as lama_kerja,
case 
	when DATEDIFF(YEAR,k.tgl_masuk, GETDATE()) >= 8 then ((j.gaji_pokok + j.tunjangan_jabatan + p.tunjangan_kinerja)*0.1) 
	else 0
	end as bonus
from karyawan as k
join pekerjaan as p
on k.nip = p.nip
join jabatan as j
on p.kd_jabatan=j.kd_jabatan
join divisi as d
on p.kode_divisi= d.kd_divisi
where DATEDIFF(YEAR,k.tgl_masuk, GETDATE()) >= 8