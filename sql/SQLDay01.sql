create database db_kampus
use db_kampus

--create tabel
create table mahasiswa(
	id bigint primary key identity (1,1),
	name varchar(50) not null,
	address varchar(50) not null,
	email varchar(255) null,
)

--ujicoba buat tabel dan tambah kolom mahasiswa_id
create table biodata(
	id bigint primary key identity (1,1),
	tanggallahir datetime not null,
	kota varchar(100)
)

alter table biodata add mahasiswa_id bigint not null

alter table biodata alter column tanggallahir date not null

--format tanggallahir dibawah ini 01/05/1999 yaitu 01 = bulan, 05 = tanggal, 1999 = tahun
INSERT INTO biodata(tanggallahir,kota,mahasiswa_id)VALUES('01/01/2000','Jakarta','3'),('02/02/2000','Jakarta','4'),('01/05/1999','Daan Mogot','1')

select * from biodata where tanggallahir = '01-05-1999'

--create view
create view vwMahasiswa
as 
select id, name, address, email from mahasiswa

select * from vwMahasiswa

--drop view
drop view vwMahasiswa

-- alter add kolom
alter table mahasiswa2 add description varchar(255)

-- alter drop clumn
alter table mahasiswa drop column description

--table alter column
alter table [mahasiswa] alter column email varchar (20)

--drop database contoh meng create databse beda

create database database2
drop database database2  

--drop table
drop table mahasiswa

--DDL
INSERT INTO mahasiswa(name,address,email)VALUES('Marcel','Daan Mogot','marcheltes@gmail.com')

INSERT INTO mahasiswa(name,address,email)
VALUES
('Toni','Garut','Toniajadeh@gmail.com'), 
('Isni','Cimahi','isnites@gmail.com')

update mahasiswa set name = 'Marchel'where id = 5;

update mahasiswa set name = 'Isni Dwitiniardi', address = 'Sumedang' where name='isni';

select * from mahasiswa;

delete from mahasiswa where id = 2;



--join table

select * , month(bio.tanggallahir)BulanLahir from mahasiswa as mhs join biodata as bio on mhs.id = bio.mahasiswa_id where mhs.name = 'toni' or bio.kota = 'jakarta' order by mhs.id asc

--order by

select * from mahasiswa as mhs join biodata as bio on mhs.id = bio.mahasiswa_id order by name asc

--select top
select top 2 * from mahasiswa order by name asc

-- between

select * from mahasiswa where id between 1 and 3 
--cara manual between
select * from mahasiswa where id >= 1 and id <=3

--like
select * from mahasiswa where name like '%s%'
select * from mahasiswa where name like 'm%l'

-- group by
select sum(id), name from mahasiswa group by name 
select count(id), name from mahasiswa group by name 
--DMO

--having server
 select count(id), name from mahasiswa group by name having count(id) > 1

 --distinct
 select distinct name from mahasiswa

--substring
select substring('sql_tutorial',1,3)

--char index
select CHARINDEX('t', 'Customer') as judul

alter table mahasiswa add panjang smallint

update mahasiswa set panjang = 50 where id = 5

update mahasiswa set panjang = 86 where name='toni'
update mahasiswa set panjang = 117 where name='isni dwitiniardi'

select * from mahasiswa order by panjang desc

-- case when
select id, name, address, panjang,
case when panjang < 50 then 'Pendek'
when panjang <= 100 then 'Sedang'
else 'Tinggi'
end as tinggiBadan
from mahasiswa order by panjang DESC

--Concat
select concat ('sql', 'is' , 'fun!')
select 'XSIS' + 'Winners'

create table penjualan(
	id bigint primary key identity (1,1),
	nama varchar(50),
	harga int
)


INSERT INTO penjualan(nama, harga)
VALUES
('Indomie', 1500),('Close-Up', 3500),('Pepsoden', 3000),('Brush Formula', 2500),('Roti Manis', 1000),('Gula', 3500),('Sarden', 4500),('Rokok sampurna', 11000),('Rokok 234', 11000)

select *, harga * 100 as [Harga * 100]from penjualan

